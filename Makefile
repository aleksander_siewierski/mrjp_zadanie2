RM=rm -f

GHC=ghc
GHCFLAGS=--make -isrc

LLVMC=llvm-as

BNFC=bnfc
BNFCFLAGS=-p $(PARSER_MODULE) -m -haskell

SRC_DIR=src
GRAMMAR_FILE=$(CURDIR)/$(SRC_DIR)/grammar/Latte.cf
BNFC_DIR=$(SRC_DIR)
PARSER_MODULE=Parser
PARSER_DIR=$(BNFC_DIR)/$(PARSER_MODULE)

SRCS:=$(wildcard $(SRC_DIR)/*.hs)
PARSER_SRCS:=$(wildcard $(PARSER_DIR)/*.hs)

.PHONY: clean parser cabal_modules latc_llvm_codegen llvm_library scripts

all: parser cabal_modules latc_llvm_codegen llvm_library scripts

parser: $(GRAMMAR_FILE)
	cd $(BNFC_DIR) && $(BNFC) $(BNFCFLAGS) $(GRAMMAR_FILE)
	$(MAKE) -C $(BNFC_DIR) # BNFC make

latc_llvm_codegen: $(SRCS) $(PARSER_SRCS)
	$(GHC) $(GHCFLAGS) src/Main.hs -o latc_llvm_codegen

cabal_modules:
	cabal update
	cabal install mtl # TODO ugly

llvm_library: lib/runtime.ll
	$(LLVMC) -o lib/runtime.bc lib/runtime.ll

scripts: src/scripts/latc.sh src/scripts/latc_llvm.sh
	cp src/scripts/latc_llvm.sh $(CURDIR)/latc_llvm
	sed -i "s:^PROJECT_ROOT=.*$$:PROJECT_ROOT=$(CURDIR):g" $(CURDIR)/latc_llvm
	chmod u+x $(CURDIR)/latc_llvm
	cp src/scripts/latc.sh $(CURDIR)/latc
	sed -i "s:^COMPILER_SCRIPT=.*$$:COMPILER_SCRIPT=$(CURDIR)/latc_llvm:g" $(CURDIR)/latc
	chmod u+x $(CURDIR)/latc

clean:
	$(RM) src/*.hi src/*.o latc_llvm_codegen latc_llvm latc lib/runtime.bc
	$(MAKE) -C $(BNFC_DIR) distclean # BNFC clean
