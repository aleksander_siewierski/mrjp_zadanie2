#/bin/bash

TEST_DIR="test"
MAIN_TESTS=$TEST_DIR/lattests
ADDITIONAL_TESTS=$TEST_DIR/mrjp-tests
MY_TESTS_DIR=$TEST_DIR/mytests

function echo_verbose {
    VERBOSE=$1
    LINE=$2

    if $VERBOSE; then
        echo "$LINE"
    fi;
}

function print_in_line {
    FILE=$1
    TEST_OUTPUT="$2"
    FILENAME_SIZE=${#FILE}
    TEST_OUTPUT_SIZE=${#TEST_OUTPUT}
    TIMES=$((80 - $FILENAME_SIZE - $TEST_OUTPUT_SIZE))
    PATTERN=%${TIMES}s
    SPACES=$(printf $PATTERN)

    echo "Executing test $FILE "${SPACES// /.}$TEST_OUTPUT
}
