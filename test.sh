#/bin/bash

. tests_commons.sh

BASIC_TESTS_DIR=$MAIN_TESTS/good

ARRAY_BASIC_TESTS=$MAIN_TESTS/extensions/arrays1
STRUCT_BASIC_TESTS=$MAIN_TESTS/extensions/struct
OBJECTS1_BASIC_TESTS=$MAIN_TESTS/extensions/objects1
OBJECTS2_BASIC_TESTS=$MAIN_TESTS/extensions/objects2

ADDITIONAL_GOOD_TESTS=$ADDITIONAL_TESTS/good
ADDITIONAL_BASIC_TESTS=$ADDITIONAL_GOOD_TESTS/basic
ADDITIONAL_ARRAY_TESTS=$ADDITIONAL_GOOD_TESTS/arrays
ADDITIONAL_VIRTUAL_TESTS=$ADDITIONAL_GOOD_TESTS/virtual

GR5_TESTS=$ADDITIONAL_TESTS/gr5

ALL_TESTS_DIRS=($BASIC_TESTS_DIR $ADDITIONAL_BASIC_TESTS $ARRAY_BASIC_TESTS $ADDITIONAL_ARRAY_TESTS $MY_TESTS_DIR $STRUCT_BASIC_TESTS
    $OBJECTS1_BASIC_TESTS $OBJECTS2_BASIC_TESTS $ADDITIONAL_VIRTUAL_TESTS $GR5_TESTS)

function perform_test {
    SOURCE_FILE=$1
    VERBOSE=$2

    LLVM_BITCODE=${SOURCE_FILE%.*}.bc
    INPUT_FILE=${SOURCE_FILE%.*}.input
    EXPECTED_OUTPUT_FILE=${SOURCE_FILE%.*}.output

    LATC_OUTPUT=`./latc -v $SOURCE_FILE 2>&1`

    if [ $? -eq 0 ]; then
        if [ -e $INPUT_FILE ]; then
            LLI_OUTPUT=`cat $INPUT_FILE | lli $LLVM_BITCODE`
        else
            LLI_OUTPUT=`lli $LLVM_BITCODE`
        fi

        if [ $? -eq 0 ]; then
            if [ ! -e $EXPECTED_OUTPUT_FILE ]; then
                echo "ERROR: output file $EXPECTED_OUTPUT_FILE does not exist"
            else
                EXPECTED_OUTPUT=`cat $EXPECTED_OUTPUT_FILE` # using this, as diff has problem with comparing file and string given with echo (newline and empty file issues)
                DIFF_OUTPUT=`diff <(echo "$EXPECTED_OUTPUT") <(echo "$LLI_OUTPUT")`
                if [ -n "$DIFF_OUTPUT" ]; then
                    echo "ERROR: diff failed!"
                    if $VERBOSE; then
                        echo "OUTPUT:"
                        echo "$LLI_OUTPUT"
                        echo -e '\nDIFF:'
                        echo "$DIFF_OUTPUT"
                    fi
                else
                    echo "OK"
                    echo_verbose $VERBOSE "$LATC_OUTPUT"
                fi
            fi
        else
            echo "ERROR: lli failed!"
            echo_verbose $VERBOSE "$LLI_OUTPUT"
        fi
    else
        echo "ERROR: latc failed!"
        echo_verbose $VERBOSE "$LATC_OUTPUT"
    fi;
}

function do_tests {
    DIR=$1

    for file in $DIR/*.lat; do
        print_in_line $file "`perform_test $file false`"
    done
}

function main {
    if [ $# -ne 0 ]; then
        SOURCE_FILE=$1

        perform_test $SOURCE_FILE true
    else
        for item in ${ALL_TESTS_DIRS[*]}; do
            do_tests $item
        done
    fi
}

make latc_llvm_codegen && main $*


