-- programs ------------------------------------------------

entrypoints Program ;

Program.    Program ::= [TopDef] ;

FnDef.	    TopDef ::= FunctionDefinition ;

TypeDef.    TopDef ::= ClassHeader "{" [FieldDeclaration] "}" ;

separator   nonempty TopDef "" ;

-- functions ------------------------------------------------

Function.   FunctionDefinition ::= Type Ident "(" [Arg] ")" Block ;

Arg. 	    Arg ::= Type Ident;

separator   Arg "," ;

-- classes -------------------------------------------------

ClassDec.    ClassHeader ::= "class" Ident;

ClassDecE.   ClassHeader ::= "class" Ident "extends" Ident;

FVar.        FieldDeclaration ::= Type Ident ";" ;

FMethod.     FieldDeclaration ::= FunctionDefinition ;

separator    FieldDeclaration "" ;

-- statements ----------------------------------------------

Block.      Block ::= "{" [Stmt] "}" ;

separator   Stmt "" ;

Empty.      Stmt ::= ";" ;

BStmt.      Stmt ::= Block ;

Decl.       Stmt ::= Type [Item] ";" ;

NoInit.     Item ::= Ident ;

Init.       Item ::= Ident "=" Expr ;

separator   nonempty Item "," ;

Ass.        Stmt ::= Var "=" Expr  ";" ;

VEntire.    Var ::= Ident ;

VComponent. Var ::= ComponentVar ;

VIndexed.   ComponentVar ::= Ident "[" Expr "]" ;

VField.     ComponentVar ::= Ident "." Ident ;

Incr.       Stmt ::= Ident "++"  ";" ;

Decr.       Stmt ::= Ident "--"  ";" ;

Ret.        Stmt ::= "return" Expr ";" ;

VRet.       Stmt ::= "return" ";" ;

Cond.       Stmt ::= "if" "(" Expr ")" Stmt  ;

CondElse.   Stmt ::= "if" "(" Expr ")" Stmt "else" Stmt  ;

While.      Stmt ::= "while" "(" Expr ")" Stmt ;

ForEach.    Stmt ::= "for" "(" Type Ident ":" Ident ")" Stmt ;

SExp.       Stmt ::= Expr  ";" ;

-- Types ---------------------------------------------------

TSimple.     Type ::= SimpleType ;

TStructured. Type ::= StructuredType ;

Void.        Type ::= "void" ;

TClass.      Type ::= Ident ;


Int.         SimpleType ::= "int" ;

Str.         SimpleType ::= "string" ;

Bool.        SimpleType ::= "boolean" ;


TArray.      StructuredType ::= SimpleType "[]" ;


internal     Fun. Type ::= Type "(" [Type] ")" ;

separator    Type "," ;

-- Expressions ---------------------------------------------

EVar.         Expr6 ::= Ident ;

ELitInt.      Expr6 ::= Integer ;

ELitTrue.     Expr6 ::= "true" ;

ELitFalse.    Expr6 ::= "false" ;

EApp.         Expr6 ::= Ident "(" [Expr] ")" ;

EString.      Expr6 ::= String ;

ENewArray.    Expr6 ::= "new" SimpleType "[" Expr "]" ;

ENewObject.   Expr6 ::= "new" Ident ;

EArray.       Expr6 ::= Ident "[" Expr "]" ;

EFieldAccess. Expr6 ::= Ident "." Ident ;

EMethodApp.   Expr6 ::= Ident "." Ident "(" [Expr] ")" ;

ENullPtr.     Expr6 ::= "(" Ident ")" "null" ;

Neg.          Expr5 ::= "-" Expr6 ;

Not.          Expr5 ::= "!" Expr6 ;

EMul.         Expr4 ::= Expr4 MulOp Expr5 ;

EAdd.         Expr3 ::= Expr3 AddOp Expr4 ;

ERel.         Expr2 ::= Expr2 RelOp Expr3 ;

EAnd.         Expr1 ::= Expr2 "&&" Expr1 ;

EOr.          Expr ::= Expr1 "||" Expr ;

coercions     Expr 6 ;

separator     Expr "," ;

-- operators -----------------------------------------------

Plus.      AddOp ::= "+" ;

Minus.     AddOp ::= "-" ;

Times.     MulOp ::= "*" ;

Div.       MulOp ::= "/" ;

Mod.       MulOp ::= "%" ;

LTH.       RelOp ::= "<" ;

LE.        RelOp ::= "<=" ;

GTH.       RelOp ::= ">" ;

GE.        RelOp ::= ">=" ;

EQU.       RelOp ::= "==" ;

NE.        RelOp ::= "!=" ;

-- comments ------------------------------------------------

comment    "#" ;

comment    "//" ;

comment    "/*" "*/" ;

