{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Exception ( catch, handle, IOException )

import System.IO ( stderr, hPutStrLn, openFile, IOMode (..), hClose )
import System.Environment ( getArgs, getProgName )
import System.Exit ( exitFailure, exitSuccess )
import System.FilePath ( replaceExtension )

import Parser.AbsLatte
import Parser.ParLatte
import Parser.PrintLatte
import Parser.ErrM

import Compiler
import Helpers


usage = do
    progName <- getProgName
    putStrLn $ "Usage: " ++ progName ++ "[-v] [FILENAME]"

exitError :: String -> IO a
exitError msg = do
  hPutStrLn stderr "ERROR"
  putStrLn msg
  exitFailure

exitOk :: String -> IO a
exitOk msg = do
  hPutStrLn stderr "OK"
  putStrLn msg
  exitSuccess


main :: IO ()
main = do
    args <- getArgs
    case args of
        fs:[]      -> processFile False fs
        "-v":fs:[] -> processFile True fs
        _          -> usage >> exitFailure

processFile :: Verbosity -> FilePath -> IO ()
processFile verbose fs =
    handle (\(e :: IOException) -> exitError $ show e) $ readFile fs >>= process Llvm verbose fs

process :: Lang -> Verbosity -> FilePath -> String -> IO ()
process lang v inputFilename fileContent =
    let ts = myLexer fileContent in
      case pProgram ts of
           Bad err  -> exitError $ "Parse failed...\nTokens:\n" ++ show ts ++ "\n" ++ show err
           Ok  tree -> processTree v lang inputFilename tree


processTree :: Verbosity -> Lang -> FilePath -> Program -> IO ()
processTree v lang inFile program = do
  -- showTree program
  (compiled, outputLog) <- processCompile v lang inFile program
  outFile <- processSave lang inFile compiled
  exitOk $ "Compiled!\nSaved to file: " ++ show outFile ++ "\n" ++ logString v outputLog


processCompile :: Verbosity -> Lang -> FilePath -> Program -> IO ([String], String)
processCompile v lang inFile program =
  let
    ((result, compiled), state) = compileProgram lang v program
   in
    case result of
      Left err       -> exitError err
      Right _        -> return (compiled, show state)


processSave :: Lang -> FilePath -> [String]-> IO String
processSave lang inFile content =
  let
    outputFile = replaceExtension inFile (getOutputFileExtension lang)
    toTry :: FilePath -> [String] -> IO String
    toTry filename fileContent = do
        h <- openFile filename WriteMode
        mapM_ (hPutStrLn h) fileContent
        hClose h
        return outputFile
    handler :: IOError -> IO String
    handler err = exitError ("Error saving to file: " ++ show err)
   in
    catch (toTry outputFile content) handler


showTree :: (Show a, Print a) => Verbosity -> a -> IO ()
showTree v tree = do
    putStrV v $ "\n[Abstract Syntax]\n\n" ++ show tree
    putStrV v $ "\n[Linearized tree]\n\n" ++ printTree tree


putStrV :: Verbosity -> String -> IO ()
putStrV True s = putStrLn s
putStrV _ _    = return ()
