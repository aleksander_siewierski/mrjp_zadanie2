module LlvmTypes where

import Helpers

import LatteTypes -- TODO I should refactorize this, so that this module will be independent from Latte definitions

-- TODO map existing Latte Type type to LlvmFirstClassType for all emit* methods
data LlvmType = -- aka First Class Type in LLVM specification
    TLSingle LlvmSingleValueType
  | TLAggregate LlvmAggregateType
   deriving (Eq, Ord)

data LlvmSingleValueType =
    TLInteger Int
  | TLPointer LlvmType
   deriving (Eq, Ord)

data LlvmAggregateType =
    TLArray LlvmType Int
  | TLStruct [LlvmType]
  | TLFunction (LlvmType, [LlvmType])
   deriving (Eq, Ord)

instance Show LlvmType where
    show (TLSingle s)    = show s
    show (TLAggregate s) = show s

instance Show LlvmSingleValueType where
    show (TLInteger bitsNo) = "i" ++ show bitsNo
    show (TLPointer ty)     = show ty ++ "*"

instance Show LlvmAggregateType where
    show (TLArray ty elemsNo)          = "[" ++ show elemsNo ++ " x " ++ show ty ++ "]"
    show (TLStruct types)              = joinList ", " $ map show types
    show (TLFunction (returnTy, args)) = show returnTy ++ "(" ++ commaJoinList (map show args) ++ ")*"


makeArrayElementsLlvmType :: LlvmType -> LlvmType
makeArrayElementsLlvmType elemTy =
    TLAggregate (TLArray elemTy 0)

makeArrayElementsPointerLlvmType :: LlvmType -> LlvmType
makeArrayElementsPointerLlvmType lTy = TLSingle (TLPointer $ makeArrayElementsLlvmType lTy)

makePointerToType :: LlvmType -> LlvmType
makePointerToType elemTy = TLSingle (TLPointer elemTy)


-------------------------------------------- LLVM register names, labels -----------------------------------------------

buildGlobalName :: String -> LlvmIdent
buildGlobalName name = GlobalIdent $ IName name

buildGlobalVariable :: Int -> LlvmIdent
buildGlobalVariable index = GlobalIdent (INameIndexed ".s" index)

buildLocalName :: String -> LlvmIdent
buildLocalName name = LocalIdent $ IName name

buildLocalVariable :: Int -> LlvmIdent -- used from local variables and registers TODO what if in Latte code there will be variable t0, t1 defined?
buildLocalVariable index = LocalIdent $ INameIndexed ".t" index

buildLabel :: String -> Int -> LlvmIdent
buildLabel name index = LocalIdent $ INameIndexed name index

functionEntryLabel :: LlvmIdent
functionEntryLabel = LocalIdent $ IName "entry"
