module SkelHelpers where

import qualified Data.List as L

import Parser.AbsLatte

-- various helper functions for traversing Latte tree


topDefsPartition :: [TopDef] -> ([TopDef], [TopDef])
topDefsPartition topDefs =
    let
        isClassDef (TypeDef _ _)   = True
        isClassDef (FnDef _)       = False
      in
        L.partition isClassDef topDefs
