module SkelLatte where

import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Writer

import qualified Data.Map   as M
import qualified Data.Maybe as Mb
import qualified Data.Set   as S

import Parser.AbsLatte
import Parser.PrintLatte

import Helpers
import TupleHelpers
import qualified NumberedMap as NM

import LatteTypes

import LlvmTypes -- TODO to refactorize
import Latte2Llvm  -- TODO to refactorize

-- Monads infrastructure:

-- means: TODO
type EvaluatedValue = ()

data Location = Loc Int deriving (Eq, Ord, Show)
type IdentsMap = M.Map Ident Location
type FunctionsMap = M.Map Ident FunctionDecl
type ClassesMap = M.Map Ident ClassBody
type ClassContext = Maybe (Ident, ClassBody)
type AllocatedObjects = M.Map LlvmIdent (Int, Type)

-- stores: TODO; mapping between declared variables IDs and values, next free space on variables map, max needed stack size for expressions
-- TODO na oko jak FreeIndexy do blokow sa w Env, to latwo sie pomieszaja, chyba jednak powinny byc w State
type Env = (IdentsMap, FunctionsMap, ClassesMap, ClassContext, Maybe FunctionDecl)

--data Function = FFunc LocEnv [FunParam] VarDeclBlock StmBlock deriving (Eq, Ord, Show)

-- stores: TODO -- stores also next location that could be taken
type Globals = NM.NumberedMap Int GlobalStringVariable
type Locations = NM.NumberedMap Location LlvmValue -- assigning next location is coordinated by Container module

type LatteState = (Globals, Locations, Int, Int, LlvmIdent, AllocatedObjects)


type LCM a = ReaderT Env (ExceptT String (WriterT [String] (StateT LatteState Identity))) a -- LCM stands for Latte Compiler Monad

type LcmProcess = LCM EvaluatedValue


type LCMResult a = ((Either String a, [String]), LatteState)

type LcmResult = LCMResult EvaluatedValue


----------------------------------------- Monads init / run, Env / State init ------------------------------------------

getLCMValue :: LCMResult a -> Either String a
getLCMValue ((res, _), _) = res


initEnv :: Env
initEnv = (M.empty, M.fromList predefinedLatteFunctions, M.empty, Nothing, Nothing)

initLatteEnvWithDecls :: FunctionsMap -> ClassesMap -> Env
initLatteEnvWithDecls functionsMap classesMap = setClassesMap classesMap . setFunctions functionsMap $ initEnv

initLatteState :: LatteState
initLatteState = (NM.singleton 0 emptyStringGlobalDef, NM.empty, 0, 0, LocalIdent $ IName "", M.empty)

runLCM :: Env -> LatteState -> LCM a-> LCMResult a
runLCM env st lcmMonad = runIdentity (runStateT (runWriterT (runExceptT (runReaderT lcmMonad env))) st)


failure :: Show a => a -> LcmProcess
failure x = throwError $ "Undefined case: " ++ show x


------------------------------------------ functions for manipulating Env ----------------------------------------------

getVariableIdents :: Env -> IdentsMap
getVariableIdents = sel1

getFunctions :: Env -> FunctionsMap
getFunctions = sel2

setFunctions :: FunctionsMap -> Env -> Env
setFunctions = upd2

setClassesMap :: ClassesMap -> Env -> Env
setClassesMap = upd3

getClassesMap :: Env -> ClassesMap
getClassesMap = sel3

getClassesMapM :: LCM ClassesMap
getClassesMapM = do
    env <- ask
    return $ sel3 env

getClass :: Ident -> Env -> Maybe ClassBody
getClass ident env =
    M.lookup ident $ getClassesMap env

getClassM :: Ident -> LCM ClassBody
getClassM ident = do
  env <- ask
  case getClass ident env of
    Nothing      -> throwError $ "No definition for class: " ++ show ident
    Just cd      -> return cd

getClassFieldM :: Ident -> Ident -> LCM (Int, Ident, Type)
getClassFieldM clsIdent fieldIdent = do
    clsBody <- getClassM clsIdent
    case M.lookup fieldIdent (classFields clsBody) of
        Nothing      -> throwError $ "No field: " ++ show fieldIdent ++ " for class: " ++ show clsIdent
        Just f       -> return f

getClassMethodM :: Ident -> Ident -> LCM (Int, Ident, Ident, FunctionDecl)
getClassMethodM clsIdent methodIdent = do
    clsBody <- getClassM clsIdent
    case M.lookup methodIdent (classMethods clsBody) of
        Nothing      -> throwError $ "No method: " ++ show methodIdent ++ " for class: " ++ show clsIdent
        Just m       -> return m

getIdentLocation :: Ident -> Env -> LCM Location
getIdentLocation ident env =
    case M.lookup ident $ getVariableIdents env of
        Nothing  -> throwError $ "Key not in map: " ++ show ident
        Just loc -> return loc

getIdentLocationM ::Ident -> LCM Location
getIdentLocationM ident = do
  env <- ask
  getIdentLocation ident env

addIdentLocation :: Ident -> Location -> Env -> LCM Env
addIdentLocation ident loc env =
    return $ upd1 (M.insert ident loc $ getVariableIdents env) env

addIdentLocationM :: Ident -> Location -> LCM Env
addIdentLocationM ident loc = do
  env <- ask
  addIdentLocation ident loc env


getFunction :: Ident -> Env -> Maybe FunctionDecl
getFunction ident env =
    M.lookup ident $ getFunctions env

getFunctionM :: Ident -> LCM FunctionDecl
getFunctionM ident = do
  env <- ask
  case getFunction ident env of
    Nothing      -> throwError $ "No identifier for " ++ show ident
    Just funDecl -> return funDecl

addFunctionM :: Ident -> FunctionDecl -> LCM Env
addFunctionM ident funDecl = do
    env <- ask
    return $ upd2 (M.insert ident funDecl $ getFunctions env) env

getClassContext :: Env -> ClassContext
getClassContext = sel4

getClassContextM :: LCM ClassContext
getClassContextM = do
    env <- ask
    return $ getClassContext env

setClassContextM :: Ident -> ClassBody -> LCM Env
setClassContextM clsIdent clsBody = do
    env <- ask
    return $ upd4 (Just (clsIdent, clsBody)) env

getVariableValueFromClassContext :: Ident -> Env -> Maybe (Ident, Int, Ident, Type)
getVariableValueFromClassContext varIdent env =
    let
        Just (clsIdent, classBody) = getClassContext env
      in
        case M.lookup varIdent (classFields classBody) of
            Nothing                 -> Nothing
            Just (idx, ownerId, ty) -> Just (clsIdent, idx, ownerId, ty)

isInClassContextM :: LCM Bool
isInClassContextM = do
    env <- ask
    return $ Mb.isJust $ getClassContext env

getFunctionDecl :: Env -> Maybe FunctionDecl
getFunctionDecl = sel5

getFunctionDeclM :: LCM (Maybe FunctionDecl)
getFunctionDeclM = do
    env <- ask
    return $ getFunctionDecl env

setFunctionDeclM :: FunctionDecl -> LCM Env
setFunctionDeclM function = do
    env <- ask
    return $ upd5 (Just function) env


------------------------------------------ functions for manipulating State --------------------------------------------

getGlobals :: LatteState -> Globals
getGlobals = sel1

setGlobals ::Globals -> LatteState -> LatteState
setGlobals = upd1

getLocations :: LatteState -> Locations
getLocations = sel2

setLocations :: Locations -> LatteState -> LatteState
setLocations = upd2

getAllocatedObjects ::LatteState -> AllocatedObjects
getAllocatedObjects = sel6

setAllocatedObjects ::AllocatedObjects -> LatteState -> LatteState
setAllocatedObjects = upd6

addGlobalM :: String -> (Int -> GlobalStringVariable) -> LCM GlobalStringVariable
addGlobalM str gStrVariableGenerator = do
    st <- get
    let
        globals = getGlobals st
        findGlobalStrDefByString globalDecl
            | str == getStringFromGlobalDecl globalDecl = True
            | otherwise                                 = False
        findExistingString = NM.findValue findGlobalStrDefByString globals
      in
        case findExistingString of
            Nothing            ->
                let
                    (_, gStrVariable, newGlobals) = NM.push (\ i -> (i, gStrVariableGenerator i)) $ getGlobals st
                  in do
                    put $ setGlobals newGlobals st
                    return gStrVariable
            Just globalStrDecl ->
                return globalStrDecl

createGlobalString :: Int -> String -> Int -> GlobalStringVariable
createGlobalString index =
  GlobalStringVariable (buildGlobalVariable index)

getGlobalsAsList :: LatteState -> [GlobalStringVariable]
getGlobalsAsList = NM.asList . getGlobals


getLocation :: Location -> LatteState -> LCM LlvmValue
getLocation loc st =
    case NM.lookup loc $ getLocations st of
        Nothing  -> throwError $ "No value for location: " ++ show loc
        Just val -> return val

getLocationM ::Location -> LCM LlvmValue
getLocationM loc = do
    st <- get
    getLocation loc st

addLocation :: LlvmValue -> LatteState -> (Location, LatteState)
addLocation llvmValue st =
    let
      (loc, _, newLocations) = NM.push (\ i -> (Loc i, llvmValue)) $ getLocations st
     in
      (loc, upd2 newLocations st)

updateLocation :: Location -> LlvmValue -> LatteState -> LCM LatteState
updateLocation loc llvmValue st =
    let
        maybeNewState = NM.setElement loc llvmValue (getLocations st) >>= \ newLocations -> Just $ upd2 newLocations st
      in
        case maybeNewState of
            Just newState -> return newState
            Nothing       -> throwError $ "Not defined location: " ++ show loc

updateLocationM :: Location -> LlvmValue -> LCM ()
updateLocationM loc llvmValue = do
    st <- get
    newState <- updateLocation loc llvmValue st
    put newState


setPrevLocationsM :: LatteState -> LcmProcess
setPrevLocationsM prevState = do
    st <- get
    put $ setLocations (getLocations prevState) st


freshRegisterIndexPool :: Int -> LatteState -> (Int, Int, LatteState) -- (start, end), right exclusive
freshRegisterIndexPool count = getFreeIndexPool sel3 count upd3

freshRegisterIndex :: LatteState -> (Int, LatteState)
freshRegisterIndex = getFreeIndex sel3 upd3

freshRegisterIndexPoolM :: Int -> LCM Int -- returns first free register index and consecutive `count` indices are also free
freshRegisterIndexPoolM count = do
    state0 <- get
    let
        (start, _, state2) = freshRegisterIndexPool count state0
      in do
        put state2
        return start


freshLabelIndexPool :: Int -> LatteState -> (Int, Int, LatteState) -- (start, end), right exclusive
freshLabelIndexPool count = getFreeIndexPool sel4 count upd4

freshLabelIndex :: LatteState -> (Int, LatteState)
freshLabelIndex = getFreeIndex sel4 upd4

freshLabelIndexM :: LCM Int
freshLabelIndexM = do
    state0 <- get
    let
        (freshLabelId, state1) = freshLabelIndex state0
      in do
        put state1
        return freshLabelId

getCurrentLabel :: LatteState -> LlvmIdent
getCurrentLabel = sel5

getCurrentLabelM :: LCM LlvmIdent
getCurrentLabelM = do
  state0 <- get
  return $ getCurrentLabel state0

setCurrentLabel :: LlvmIdent -> LatteState -> LatteState
setCurrentLabel = upd5

setCurrentLabelM :: LlvmIdent -> LCM ()
setCurrentLabelM label = do
  state0 <- get
  put $ setCurrentLabel label state0

addAllocatedObjectM :: LlvmIdent -> Type -> LcmProcess
addAllocatedObjectM rn ty = do
    state0 <- get
    put $ setAllocatedObjects (M.insert rn (1, ty) $ getAllocatedObjects state0) state0

abstractChangeAllocatedObjectM :: Int -> LlvmIdent -> LCM Bool
abstractChangeAllocatedObjectM i rn = do
    state0 <- get
    let
        allocatedObjects = getAllocatedObjects state0
        (refCount, ty) = allocatedObjects M.! rn
        newRefCount = refCount + i
        newAllocatedObjects =
            if
                newRefCount == 0
              then
                M.delete rn allocatedObjects
              else
                M.insert rn (newRefCount, ty) allocatedObjects
      in do
        put $ setAllocatedObjects newAllocatedObjects state0
        return $ newRefCount == 0

incAllocatedObjectM :: LlvmIdent -> LCM Bool
incAllocatedObjectM = abstractChangeAllocatedObjectM 1

decAllocatedObjectM :: LlvmIdent -> LCM Bool
decAllocatedObjectM = abstractChangeAllocatedObjectM (-1)


---------------- convenient functions acting on both Env and State, mostly variables -----------------------------------

getIdentValue :: Ident -> Env -> LatteState -> LCM LlvmValue
getIdentValue ident env state0 =
    getIdentLocation ident env >>= \ loc -> getLocation loc state0

getIdentValueM ::Ident -> LCM LlvmValue
getIdentValueM ident = do
    env <- ask
    state0 <- get
    getIdentValue ident env state0

updateIdentValue :: Ident -> LlvmValue -> Env -> LatteState -> LCM LatteState
updateIdentValue ident val env state0 =
    getIdentLocation ident env >>= \ loc -> updateLocation loc val state0

updateIdentValueM :: Ident -> LlvmValue -> LcmProcess
updateIdentValueM ident val = do
    env <- ask
    state0 <- get
    newState <- updateIdentValue ident val env state0
    put newState

addIdentValue :: Ident -> LlvmValue -> Env -> LatteState -> LCM (Env, LatteState)
addIdentValue ident value env state0 =
    let
        (loc, state2) = addLocation value state0
      in do
        env2 <- addIdentLocation ident loc env
        return (env2, state2)

addIdentValueM :: Ident -> LlvmValue -> LCM Env
addIdentValueM ident value = do
    env <- ask
    state0 <- get
    (env2, state2) <- addIdentValue ident value env state0
    put state2
    return env2


getIdentValues :: S.Set Ident -> LCM (M.Map Ident LlvmValue)
getIdentValues idents = do
    env <- ask
    state0 <- get
    keyValues <- mapM (
        \ ident -> do
            lVal <- getIdentValue ident env state0
            return (ident, lVal)
        ) $ S.toList idents
    return $ M.fromList keyValues


------------------------ helper methods that do lookout on both environment and class context --------------------------

resolveGetIdentM :: Ident -> LCM (Either (Ident, Int, Ident, Type) LlvmValue) -- TODO describe this behaviour
resolveGetIdentM varIdent = do
    classContext <- isInClassContextM
    if
        classContext
      then
        if
            varIdent == thisPointerIdent
          then do
            Just (clsIdent, _) <- getClassContextM
            return $ Right $ resolveThis clsIdent
          else do
            env <- ask
            case getVariableValueFromClassContext varIdent env of
                Just v  -> return $ Left v
                Nothing -> do
                    val <- getIdentValueM varIdent
                    return $ Right val
      else do
        val <- getIdentValueM varIdent
        return $ Right val

resolveSetIdentM :: Ident -> LCM (Either (Ident, Int, Ident, Type) LlvmValue) -- TODO describe this behaviour
resolveSetIdentM varIdent = do
    classContext <- isInClassContextM
    if
        classContext
      then do
        env <- ask
        case getVariableValueFromClassContext varIdent env of
            Just v  -> return $ Left v
            Nothing -> do
                val <- getIdentValueM varIdent
                return $ Right val
      else do
        val <- getIdentValueM varIdent
        return $ Right val

resolveThis :: Ident -> LlvmValue
resolveThis clsIdent =
    TRegisterName (TClass clsIdent) thisPointerLlvmIdent


safeGetIdentValueM ::Ident -> LCM (Maybe LlvmValue) -- TODO describe this behaviour
safeGetIdentValueM ident = do
    eitherIdentVal <- resolveGetIdentM ident
    case eitherIdentVal of
        Left _    -> return Nothing
        Right val ->
            case val of
                TIntConstant _             -> return $ Just val
                TBoolConstant _            -> return $ Just val
                TRegisterString _ (Just _) -> return $ Just val
                _                          -> return Nothing


----------------------------------------- Some other Latte components --------------------------------------------------

predefinedLatteFunctions :: [(Ident, FunctionDecl)]
predefinedLatteFunctions = [
    (Ident "printString", (Void, [(TSimple Str, Ident "str")])),
    (Ident "printInt", (Void, [(TSimple Int, Ident "int")])),
    (Ident "error", (Void, [])),
    (Ident "readInt", (TSimple Int, [])),
    (Ident "readString", (TSimple Str, []))
  ]

latteMaxInt :: Integer
latteMaxInt = 2 ^ 32 - 1

mainFunctionDefinition :: (Ident, FunctionDecl)
mainFunctionDefinition = (Ident "main", (TSimple Int, []))

----------------------------------------------- Helper functions -------------------------------------------------------

getFreeIndexPool :: (a -> Int)-> Int -> (Int -> a -> a)-> a -> (Int, Int, a) -- (start, end), right exclusive
getFreeIndexPool firstFreeIndexGenerator count updateFreeIndex container =
    let
      startIndex = firstFreeIndexGenerator container
      endIndex = startIndex + count
      newContainer = updateFreeIndex endIndex container
     in
      (startIndex, endIndex, newContainer)

getFreeIndex :: (a -> Int) -> (Int -> a -> a) -> a -> (Int, a)
getFreeIndex firstFreeIndexGenerator updateFreeIndex container =
  let
    (start, _, newContainer) = getFreeIndexPool firstFreeIndexGenerator 1 updateFreeIndex container
   in
    (start, newContainer)

------------------------------------------------- print functions ------------------------------------------------------

environmentPrettyPrint :: Env -> String
environmentPrettyPrint (identsMap, functionsMap, classes, classContext, fun) =
    unlines ["identifiers:", mapPrettyPrint identsMap, "functions:", mapPrettyPrint functionsMap, "classes:",
            mapPrettyPrint classes, "class context:", classContextPrettyPrint classContext, "function:", show fun]

classContextPrettyPrint :: ClassContext -> String
classContextPrettyPrint Nothing = "Empty"
classContextPrettyPrint (Just (clsIdent, clsBody)) =
    unlines ["Class:", printTree clsIdent, "parentClass:", show $ parentClass clsBody, "fields:",
            mapPrettyPrint $ classFields clsBody, "methods:", mapPrettyPrint $ classMethods clsBody]


----------------------------------------------- Latte tree helpers -----------------------------------------------------

getClassIdent :: ClassHeader -> Ident
getClassIdent (ClassDec ident)    = ident
getClassIdent (ClassDecE ident _) = ident

getClassParent :: ClassHeader -> Maybe Ident
getClassParent (ClassDec _)            = Nothing
getClassParent (ClassDecE _ parentCls) = Just parentCls
