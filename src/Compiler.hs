module Compiler where

import Parser.AbsLatte

import Helpers

import SkelLatte
import LlvmCompiler as LlvmC

data Lang = Llvm

-- functions parametrized by output language, to change when other languages will be introduced


compileProgram :: Lang -> Verbosity -> Program -> LcmResult
compileProgram Llvm = LlvmC.compile


getOutputFileExtension :: Lang -> String
getOutputFileExtension Llvm = LlvmC.outputFileExtension