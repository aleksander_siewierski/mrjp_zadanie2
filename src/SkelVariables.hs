module SkelVariables where

import           Control.Monad.Except
import qualified Data.Set             as S

import           Parser.AbsLatte
import           Parser.PrintLatte

import           Helpers

import           LatteTypes

import           SkelLatte


-------- various functions for counting vital variables and variable definition as defined in a lecture ----------------


-- here means variables that before calling a stmt must be known and later can change, so we must introduce some Phi nodes for them
findVitalNonReachingVariablesDefinitions :: Stmt -> S.Set Ident
findVitalNonReachingVariablesDefinitions stmt = findVitalVariables stmt `S.intersection` findVariableDefinitions stmt

findVitalVariables :: Stmt -> S.Set Ident
findVitalVariables (BStmt block) =
    case block of
        Block stmts ->
            foldl findInVariables S.empty $ reverse stmts
findVitalVariables stmt = findInVariables S.empty stmt

findVariableDefinitions :: Stmt -> S.Set Ident
findVariableDefinitions (BStmt block) =
    case block of
        Block stmts ->
            foldl findOutVariables S.empty stmts
findVariableDefinitions stmt = findOutVariables S.empty stmt


findOutVariables :: S.Set Ident -> Stmt -> S.Set Ident
findOutVariables inVariables stmt =
    findGenVariables stmt `S.union` (inVariables S.\\ findKillVariables stmt)


findInVariables :: S.Set Ident -> Stmt -> S.Set Ident
findInVariables outVariables stmt =
    outVariables S.\\ findKillVariables stmt `S.union` findUseVariables stmt

findKillVariables :: Stmt -> S.Set Ident
findKillVariables (Ass (VEntire ident) _) = S.singleton ident
findKillVariables (Decl _ items)          = setMap findKillVariablesItem items
findKillVariables (BStmt block)           = findKillVariablesBlock block
findKillVariables (Incr ident)            = S.singleton ident
findKillVariables (Decr ident)            = S.singleton ident
findKillVariables _                       = S.empty

-- TODO Cond CondElse i While daja puste zbiory kill, bo nie mamy pewnosci czy wejdzie do stmt (ewentualnie je uproscic)

findKillVariablesBlock :: Block -> S.Set Ident
findKillVariablesBlock (Block stmts) =
    setFold findKillVariables $ reverse stmts -- TODO prawdopodobnie do poprawy

getItemIdent :: Item -> Ident
getItemIdent (NoInit ident) = ident
getItemIdent (Init ident _) = ident

findKillVariablesItem :: Item -> Ident
findKillVariablesItem = getItemIdent

findUseVariables :: Stmt -> S.Set Ident
findUseVariables (BStmt block)               = findUseVariablesBlock block
findUseVariables (Ass var expr)              =
    let
        findUseVariablesVar =
            case var of
                VEntire _                         -> S.empty
                VComponent (VIndexed _ indexExpr) -> findUseVariablesExpr indexExpr
      in
        findUseVariablesVar `S.union` findUseVariablesExpr expr
findUseVariables (Incr ident)                = S.singleton ident
findUseVariables (Decr ident)                = S.singleton ident
findUseVariables (Ret expr)                  = findUseVariablesExpr expr
findUseVariables (Cond expr stmt)            = findUseVariablesExpr expr `S.union` findUseVariables stmt
findUseVariables (CondElse expr stmt0 stmt1) = S.unions [findUseVariablesExpr expr, findUseVariables stmt0, findUseVariables stmt1]
findUseVariables (While expr stmt)           = findUseVariablesExpr expr `S.union` findUseVariables stmt
findUseVariables (SExp expr)                 = findUseVariablesExpr expr
findUseVariables _                           = S.empty

findUseVariablesBlock :: Block -> S.Set Ident
findUseVariablesBlock (Block stmts) = setFold findUseVariables stmts

findUseVariablesItem :: Item -> S.Set Ident
findUseVariablesItem (NoInit _)    = S.empty
findUseVariablesItem (Init _ expr) = findUseVariablesExpr expr

findUseVariablesExpr :: Expr -> S.Set Ident
findUseVariablesExpr (EVar ident)       = S.singleton ident
findUseVariablesExpr (EApp _ exprs)     = setFold findUseVariablesExpr exprs
findUseVariablesExpr (ENewArray _ expr) = findUseVariablesExpr expr
findUseVariablesExpr (EArray _ expr)    = findUseVariablesExpr expr
findUseVariablesExpr (Neg expr)         = findUseVariablesExpr expr
findUseVariablesExpr (Not expr)         = findUseVariablesExpr expr
findUseVariablesExpr expr               =
    let
        exprOperatorHelper expr0 expr1 = S.union (findUseVariablesExpr expr0) (findUseVariablesExpr expr1)
      in
        case expr of
            EMul e0 _ e1 -> exprOperatorHelper e0 e1
            EAdd e0 _ e1 -> exprOperatorHelper e0 e1
            ERel e0 _ e1 -> exprOperatorHelper e0 e1
            EAnd e0 e1   -> exprOperatorHelper e0 e1
            EOr e0 e1    -> exprOperatorHelper e0 e1
            _            -> S.empty

findGenVariables :: Stmt -> S.Set Ident
findGenVariables (BStmt block)            = findGenVariablesBlock block
findGenVariables (Ass (VEntire ident) _)  = S.singleton ident
findGenVariables (Decl _ items)           = setMap findGenVariablesItem items
findGenVariables (Incr ident)             = S.singleton ident
findGenVariables (Decr ident)             = S.singleton ident
findGenVariables (Cond _ stmt)            = findGenVariables stmt
findGenVariables (CondElse _ stmt0 stmt1) = findGenVariables stmt0 `S.union` findGenVariables stmt1
findGenVariables (While _ stmt)           = findGenVariables stmt
findGenVariables _                        = S.empty


findGenVariablesBlock :: Block -> S.Set Ident
findGenVariablesBlock (Block stmts) =
    setFold findGenVariables stmts

findGenVariablesItem :: Item -> Ident
findGenVariablesItem = getItemIdent


---------------------------------------- some other Stmt / Expr helpers ------------------------------------------------

{-
  Tells whether given expression has constant value in context of given non reaching variables definitions set (set of
  variables that are vital and later can be redefined), so expression is constant if for each used variable, variable
  has constant value, and this variable is not member of non reaching definitions set
-}
maybeConstExprInNonReachingDefsContext :: Expr -> S.Set Ident -> LCM (Maybe LlvmValue)
maybeConstExprInNonReachingDefsContext (EVar ident) nonReachingDefinitions = do
    maybeVal <- safeGetIdentValueM ident
    case maybeVal of
        Nothing  -> return Nothing
        Just val ->
            if
                isConstantValue val && S.notMember ident nonReachingDefinitions
              then
                return $ Just val
              else
                return Nothing

maybeConstExprInNonReachingDefsContext (EString _) _                       = return Nothing -- TODO
maybeConstExprInNonReachingDefsContext (Neg expr) nonReachingDefinitions   = do
    maybeVal <- maybeConstExprInNonReachingDefsContext expr nonReachingDefinitions
    case maybeVal of
        Just (TIntConstant n) -> return $ Just (TIntConstant (-n))
        _                     -> return maybeVal
    -- fmap (\ (TIntConstant n) -> TIntConstant -n) $ maybeConstExprInNonReachingDefsContext expr nonReachingDefinitions
maybeConstExprInNonReachingDefsContext (Not expr) nonReachingDefinitions   = do
    maybeVal <-  maybeConstExprInNonReachingDefsContext expr nonReachingDefinitions
    case maybeVal of
        Just (TBoolConstant b) -> return $ Just (TBoolConstant $ not b)
        _                      -> return maybeVal
    -- fmap (\ (TBoolConstant b) -> TBoolConstant $ not b) $ maybeConstExprInNonReachingDefsContext expr nonReachingDefinitions

maybeConstExprInNonReachingDefsContext expr nonReachingDefinitions         =
    let
        maybeConstIntExprHelper :: (Int -> Int -> Int) -> Expr -> Expr -> S.Set Ident -> LCM (Maybe LlvmValue)
        maybeConstIntExprHelper operation expr0 expr1 nonReachingDefs = do
            v0 <- maybeConstExprInNonReachingDefsContext expr0 nonReachingDefs
            v1 <- maybeConstExprInNonReachingDefsContext expr1 nonReachingDefs
            case (v0, v1) of
                (Just (TIntConstant n0), Just (TIntConstant n1)) -> return $ Just (TIntConstant $ operation n0 n1)
                _                                                -> return Nothing
        maybeConstRelOpExprHelper :: RelOp -> Expr -> Expr -> S.Set Ident -> LCM (Maybe LlvmValue) -- TODO use same as for && / ||
        maybeConstRelOpExprHelper relOp expr0 expr1 nonReachingDefs = do
            v0 <- maybeConstExprInNonReachingDefsContext expr0 nonReachingDefs
            v1 <- maybeConstExprInNonReachingDefsContext expr1 nonReachingDefs
            case (v0, v1) of
                (Just (TIntConstant n0), Just (TIntConstant n1))   -> return $ Just (TBoolConstant $ getRelOperator relOp n0 n1)
                (Just (TBoolConstant b0), Just (TBoolConstant b1)) -> return $ Just (TBoolConstant $ getRelOperator relOp b0 b1)
                    -- let
                    --     opResult = operation 1 1
                    --   in
                    --     return $ Just (TBoolConstant $ opResult)
                _                                                  -> return Nothing
        maybeConstOrBoolExprHelper :: Expr -> Expr -> S.Set Ident -> LCM (Maybe LlvmValue) -- TODO use same as for && / ||
        maybeConstOrBoolExprHelper expr0 expr1 nonReachingDefs = do
            maybeV0 <- maybeConstExprInNonReachingDefsContext expr0 nonReachingDefs
            case maybeV0 of
                Just (TBoolConstant True)  -> return maybeV0
                Just (TBoolConstant False) ->
                    maybeConstExprInNonReachingDefsContext expr1 nonReachingDefs
                _                          -> return Nothing
        maybeConstAndBoolExprHelper :: Expr -> Expr -> S.Set Ident -> LCM (Maybe LlvmValue) -- TODO use same as for && / ||
        maybeConstAndBoolExprHelper expr0 expr1 nonReachingDefs = do
            maybeV0 <- maybeConstExprInNonReachingDefsContext expr0 nonReachingDefs
            case maybeV0 of
                Just (TBoolConstant False)  -> return maybeV0
                Just (TBoolConstant True)   ->
                     maybeConstExprInNonReachingDefsContext expr1 nonReachingDefs
                _                           -> return Nothing
      in
        case expr of
            EMul e0 op e1        -> maybeConstIntExprHelper (getMulOperator op) e0 e1 nonReachingDefinitions
            EAdd e0 op e1        -> maybeConstIntExprHelper (getAddOperator op) e0 e1 nonReachingDefinitions
            ERel e0 op e1        -> maybeConstRelOpExprHelper op e0 e1 nonReachingDefinitions
            EAnd e0 e1           -> maybeConstAndBoolExprHelper e0 e1 nonReachingDefinitions
            EOr e0 e1            -> maybeConstOrBoolExprHelper e0 e1 nonReachingDefinitions
            EApp _ _             -> return Nothing
            EMethodApp _ _ _     -> return Nothing
            EFieldAccess ident _ -> do
                maybeIdentVal <- safeGetIdentValueM ident
                case maybeIdentVal of
                    Nothing       -> return Nothing
                    Just identVal ->
                        case identVal of
                            TRegisterArray _ _ maybeArrayBody ->
                                case maybeArrayBody of
                                    Just (sizeVal@(TIntConstant _), _) -> return $ Just sizeVal
                                    _                                  -> return Nothing
                            _                                 -> return Nothing
            _                    -> do
                constVal <-transConstantExpr expr
                return $ Just constVal

transConstantExpr :: Expr -> LCM LlvmValue
transConstantExpr (ELitInt n)              = return $ TIntConstant $ fromIntegral n
transConstantExpr ELitTrue                 = return $ TBoolConstant True
transConstantExpr ELitFalse                = return $ TBoolConstant False
transConstantExpr (ENullPtr ident)         = return $ TNullPointer (TClass ident)
transConstantExpr expr                     = throwError $ "Unknown constant expression type: " ++ printTree expr

isConstantExpr :: Expr -> Bool
isConstantExpr (ELitInt _)      = True
isConstantExpr ELitTrue         = True
isConstantExpr ELitFalse        = True
isConstantExpr (ENullPtr _)     = True
isConstantExpr _                = False


maybeConstExpr :: Expr -> LCM (Maybe LlvmValue)
maybeConstExpr expr = maybeConstExprInNonReachingDefsContext expr S.empty


-- getAddOperator :: Integral a => AddOp -> (a -> a -> a)
getAddOperator :: AddOp -> Int -> Int -> Int
getAddOperator Plus  = (+)
getAddOperator Minus = (-)

-- getMulOperator :: Integral a => MulOp -> (a -> a -> a)
getMulOperator :: MulOp -> Int -> Int -> Int
getMulOperator Times = (*)
getMulOperator Div   = div
getMulOperator Mod   = rem

getRelOperator :: (Ord a, Eq a) => RelOp -> a -> a -> Bool
getRelOperator op =
    case op of
        LTH -> (<)
        LE  -> (<=)
        GTH -> (>)
        GE  -> (>=)
        EQU -> (==)
        NE  -> (/=)
