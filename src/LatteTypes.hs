module LatteTypes where

import qualified Data.Map as M

import Helpers

import Parser.AbsLatte as Latte


data IdentName =
   INameIndexed String Int
 | IName String
 deriving (Eq, Ord)

instance Show IdentName  where
  show (INameIndexed name index) = name ++ show index
  show (IName name) = name

data LlvmIdent =
   LocalIdent IdentName
 | GlobalIdent IdentName
 deriving (Eq, Ord)

instance Show LlvmIdent where
  show (LocalIdent name) = "%" ++ show name
  show (GlobalIdent name) = "@" ++ show name

type ArrayBody = Maybe (LatteValue, LlvmIdent)

data LatteValue =
    NotInitialized SimpleType -- means Array that is not initialized yet
  | TVoidConstant
  | TIntConstant Int
  | TBoolConstant Bool
  | TRegisterName Type LlvmIdent
  | TRegisterArray SimpleType LlvmIdent ArrayBody
  | TRegisterString LlvmIdent (Maybe GlobalStringVariable)
  | TNullPointer Type
   deriving (Eq,Ord,Show)

type LlvmValue = LatteValue -- TODO name deprecated, use LatteValue

type FunctionArg  = (Latte.Type, Ident)
type FunctionDecl = (Latte.Type, [FunctionArg])

type FieldsMap = M.Map Ident (Int, Ident, Type)
type MethodsMap = M.Map Ident (Int, Ident, Ident, FunctionDecl)

data ClassBody = ClassBody { parentClass  :: Maybe Ident
                           , classFields  :: FieldsMap
                           , classMethods :: MethodsMap
                           }
instance Show (ClassBody) where
    show clsBody = unlines ["parentClass: " ++ show (parentClass clsBody), "fields:", mapPrettyPrint $ classFields clsBody, "methods:", mapPrettyPrint $ classMethods clsBody]


data GlobalStringVariable =
    GlobalStringVariable LlvmIdent String Int
   deriving (Eq,Ord,Show)

intType :: Type
intType = TSimple Int

boolType :: Type
boolType = TSimple Bool

aliasTString :: Type
aliasTString = TSimple Str

makeArrayType :: SimpleType -> Type
makeArrayType simpleTy = TStructured (TArray simpleTy)

emptyStringRn :: LlvmIdent
emptyStringRn = GlobalIdent (IName ".empty_string")

emptyStringGlobalDef :: GlobalStringVariable
emptyStringGlobalDef = GlobalStringVariable emptyStringRn "" 1

defaultValue :: Type -> LlvmValue
defaultValue (TSimple Int)             = TIntConstant 0
defaultValue (TSimple Bool)            = TBoolConstant False
defaultValue (TStructured (TArray ty)) = NotInitialized ty
defaultValue ty@(TClass _)             = TNullPointer ty

defaultStringValue :: LlvmIdent -> LlvmValue
defaultStringValue emptyStrPtrRn       = TRegisterString emptyStrPtrRn $ Just emptyStringGlobalDef

latteValue2Type :: LatteValue -> Type
latteValue2Type (NotInitialized simpleTy)                      = TStructured (TArray simpleTy)
latteValue2Type TVoidConstant                                  = Void
latteValue2Type (TBoolConstant _)                              = TSimple Bool
latteValue2Type (TIntConstant _)                               = TSimple Int
latteValue2Type (TRegisterName t _)                            = t
latteValue2Type (TRegisterArray t _ _)                         = TStructured (TArray t)
latteValue2Type (TRegisterString _ _)                          = TSimple Str
latteValue2Type (TNullPointer ty)                              = ty

llvmValue2LlvmType :: LlvmValue -> Type -- TODO deprecated
llvmValue2LlvmType = latteValue2Type

latteType2Value :: Type -> LlvmIdent -> LatteValue
latteType2Value (TStructured (TArray t)) rn          = TRegisterArray t rn Nothing
latteType2Value (TSimple Str) rn                     = TRegisterString rn Nothing
latteType2Value ty rn                                = TRegisterName ty rn


getIdentName :: Ident -> String
getIdentName (Ident name) = name

isString :: LlvmValue -> Bool
isString (TRegisterString _ _)           = True
isString _                               = False

isConstantValue :: LlvmValue -> Bool
isConstantValue (TBoolConstant _)            = True
isConstantValue (TIntConstant _)             = True
isConstantValue (TRegisterString _ (Just _)) = True
isConstantValue _                            = False

isPointerLlvmType :: Type -> Bool
isPointerLlvmType (TSimple Int)  = False
isPointerLlvmType (TSimple Bool) = False
isPointerLlvmType _              = True

getStringLen :: LlvmValue -> Maybe Int
getStringLen (TRegisterString _ (Just (GlobalStringVariable _ _ size))) = Just size
getStringLen _                                                          = Nothing

getRegIdent :: LlvmValue -> LlvmIdent
getRegIdent (TRegisterName _ regName)    = regName
getRegIdent (TRegisterString regName _)  = regName
getRegIdent (TRegisterArray _ regName _) = regName

buildRegValue :: Type -> LlvmIdent -> LlvmValue
buildRegValue = TRegisterName

getStringFromGlobalDecl :: GlobalStringVariable -> String
getStringFromGlobalDecl (GlobalStringVariable _ str _) = str

getSizeFromGlobalDecl :: GlobalStringVariable -> Int
getSizeFromGlobalDecl (GlobalStringVariable _ _ size) = size


isProperRValue :: LatteValue -> Bool
isProperRValue (NotInitialized _)                             = False
isProperRValue TVoidConstant                                  = False
isProperRValue _                                              = True


classConstructorName :: Ident -> Ident
classConstructorName (Ident cls) =
    Ident $ cls ++ "_Create_Default"

classVTableType :: Ident -> Ident
classVTableType (Ident cls) =
    Ident $ cls ++ "_vtable_type"

classVTableRn :: Ident -> Ident
classVTableRn (Ident cls) =
    Ident $ cls ++ "_vtable_data"

methodName :: Ident -> Ident -> Ident
methodName (Ident cls) (Ident method) =
    Ident $ cls ++ "_" ++ method

thisPointerIdent :: Ident
thisPointerIdent = Ident thisPointerName

thisPointerName :: String
thisPointerName = "self"

function2Type :: FunctionDecl -> Type
function2Type (returnTy, args) =
    Fun returnTy $ map fst args
