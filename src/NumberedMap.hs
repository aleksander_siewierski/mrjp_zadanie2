module NumberedMap where

-- kind of Map, that keys (and sometimes values) have direct dependency from Integers, so that collection itself manages generating integers for keys
-- and values (if necessary).

import qualified Data.Map as M

empty ::NumberedMap k v
singleton :: (Ord k) => k -> v -> NumberedMap k v
push :: (Ord k) => (Int -> (k, v)) -> NumberedMap k v -> (k, v, NumberedMap k v)
asList :: NumberedMap k v -> [v]
lookup :: (Ord k) => k -> NumberedMap k v -> Maybe v
(!) :: Ord k => NumberedMap k a -> k -> a
getElement :: (Ord k) => k -> NumberedMap k v -> v
setElement :: (Ord k) => k -> v -> NumberedMap k v -> Maybe (NumberedMap k v)
findValue ::(v -> Bool) -> NumberedMap k v -> Maybe v

getElement k nm = nm NumberedMap.! k

data NumberedMap k v = NumberedMapImpl (M.Map k v) Int

instance (Show k, Show v) => Show (NumberedMap k v) where
    show (NumberedMapImpl _ 0) = "NumberedMap: empty"
    show (NumberedMapImpl m i      ) =
        "\nNumberedMap: last index is " ++ show i ++ ", elements:" ++ M.foldlWithKey (\ str k v -> str ++ "\n" ++ show k ++ " : " ++ show v) "" m ++ "\n"

empty = NumberedMapImpl M.empty 0

singleton k v =
    NumberedMapImpl (M.insert k v M.empty) 1

push keyValueGenerator (NumberedMapImpl m nextIndex) =
    let
      (k, v) = keyValueGenerator nextIndex
     in
      (k, v, NumberedMapImpl (M.insert k v m) (nextIndex + 1))

asList (NumberedMapImpl m _) = M.elems m

lookup k (NumberedMapImpl m _) = M.lookup k m

(!) (NumberedMapImpl m _) k = m M.! k

setElement k v (NumberedMapImpl m i) =
    M.lookup k m >> Just (NumberedMapImpl (M.insert k v m) i)

findValue predicate (NumberedMapImpl m _) =
    case M.elems (M.filter predicate m) of
        []    -> Nothing
        (e:_) -> Just e
