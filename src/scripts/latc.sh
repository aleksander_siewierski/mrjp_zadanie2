#!/bin/sh

COMPILER_SCRIPT="./latc_llvm.sh"

if [ $# -eq 1 ]; then
    $COMPILER_SCRIPT $1
elif [ $# -eq 2 -a $1 = "-v" ]; then
    $COMPILER_SCRIPT $1 $2
else
    echo "Usage: $0 [-v] FILE"
    exit 255
fi;