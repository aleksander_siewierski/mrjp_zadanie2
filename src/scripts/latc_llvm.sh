#!/bin/sh

PROJECT_ROOT=/home/olek/projects/MRJP/

LATTE_COMPILER=$PROJECT_ROOT/latc_llvm_codegen

LIBRARY_FILE=$PROJECT_ROOT/lib/runtime.bc

LLVM_AS_CMD="llvm-as"
LLVM_LINK_CMD="llvm-link"

if [ $# -eq 1 ]; then
    FILE=$1
    VERBOSE=false
elif [ $# -eq 2 ]; then
    FILE=$2
    VERBOSE=true
else
    echo "Usage: $0 [-v] FILE"
    exit 255
fi


FILE_DIR=`dirname $FILE`
FILENAME=${FILE##*/} # only name of file, without path

if $VERBOSE; then
    $LATTE_COMPILER -v $FILE
else
    $LATTE_COMPILER $FILE
fi

if [ $? -ne 0 ]; then
    #echo $RES_CODEGEN;
    exit 255;
fi;

LL_FILE="${FILE%.*}.ll"
LL_BITCODE_FILE="${FILE%.*}_tmp.bc" # changing file extension
LL_LINKED_FILE="${FILE%.*}.bc" # same as above

RES_LLVM_AS=`$LLVM_AS_CMD -o $LL_BITCODE_FILE $LL_FILE`

if [ $? -ne 0 ]; then
    echo "ERROR\n" >&2
    echo $RES_LLVM_AS
    exit 255
fi

RES_LLVM_LINK=`$LLVM_LINK_CMD -o $LL_LINKED_FILE $LL_BITCODE_FILE $LIBRARY_FILE`
LINK_RESULT=$?

rm -f $LL_BITCODE_FILE
if [ $LINK_RESULT -ne 0 ]; then
    echo "ERROR\n" >&2
    echo $RES_LLVM_LINK
    exit 255
else
    exit 0
fi
