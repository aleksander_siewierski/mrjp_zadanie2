module LlvmCompiler ( outputFileExtension, compile ) where

import Helpers

import Parser.AbsLatte

import LatteTypeChecker as TC

import LlvmEmiter

import SkelLatte
import SkelLlvm

outputFileExtension :: String
outputFileExtension = ".ll"


doTypeChecking :: Verbosity -> Program -> LCMResult (FunctionsMap, ClassesMap)
doTypeChecking v program =
  let
    typeCheckerRes =  TC.checkTypes program
    ((res, logs), state) = typeCheckerRes
   in
    case res of
      Left err ->
          let
              verboseMessage = "\n\nState:\n" ++ show state ++ "\nLogs:\n" ++ printLines logs
            in
              ((Left $ "TypeChecker error...\n" ++ err ++ logString v verboseMessage, logs), state)
      _        -> typeCheckerRes

compile :: Verbosity -> Program -> LcmResult
compile v program =
  let
    ((res, logs), tcState) = doTypeChecking v program
   in
    case res of
      Left err                         -> ((Left err, logs), tcState)
      Right (functionsMap, classesMap) ->
        case runLCM (initLatteEnvWithDecls functionsMap classesMap) initLatteState $ transProgram program of
            ((Right (), stmts), state) ->
              ((Right (), finalStep stmts state), state)
            ((Left err, stmts), state) ->
              let
                verboseMessage = "\nState:\n" ++ show state ++ "\n\nStatements:" ++ printLines stmts ++ "\n" -- ++ show program
               in
                ((Left $ "Compilation error...\n" ++ err ++ logString v verboseMessage, stmts), state)


finalStep :: [String] -> LatteState -> [String]
finalStep body state = emitProgramPrologue (getGlobalsAsList state) ++ body
