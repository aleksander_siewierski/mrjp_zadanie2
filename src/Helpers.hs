module Helpers where

import Data.Char
import Data.List
import qualified Data.Set as S
import qualified Data.Map as M

import Numeric


type Verbosity = Bool


comma :: String
comma = ", "

logString :: Verbosity -> String -> String
logString False _ = ""
logString _ str   = str


findDuplicates :: Eq a => [a] -> [a]
findDuplicates list =
    list \\ nub list


joinList :: String -> [String] -> String
joinList _ [] = ""
joinList sep (l:ls) =
    foldl (\ r x-> r ++ sep ++ x) l ls

commaJoinList :: [String] -> String
commaJoinList = joinList comma

enumerateListFrom :: (Enum t, Num t) => t -> [a] -> [(t, a)]
enumerateListFrom idx = zip [idx..]

enumerateList :: (Enum t, Num t) => [a] -> [(t, a)]
enumerateList = enumerateListFrom 0

setMap :: Ord b => (a -> b) -> [a] -> S.Set b
setMap f = foldl (\ bs b -> S.insert (f b) bs) S.empty

setFold :: Ord b => (a -> S.Set b) -> [a] -> S.Set b
setFold f = foldl (\ prevBs a -> prevBs `S.union` f a) S.empty


getEscapedString :: String -> String
getEscapedString str =
    let
        escapeChar :: Char -> String
        escapeChar ch
            | isAlphaNum ch = [ch]
            | otherwise     =
                let
                    addZeroPrefix = ord ch < 16
                    prefix = if addZeroPrefix then "0" else ""
                  in
                    "\\" ++ prefix ++ showHex (ord ch) ""
      in
        foldl (\ mappedStr ch -> mappedStr ++ escapeChar ch) "" str

printLines :: [String] -> String
printLines = foldl (\ str line -> str ++ "\n" ++ line) ""


mapPrettyPrint :: (Show k, Show v) => M.Map k v -> String
mapPrettyPrint =
    M.foldlWithKey (\ str k v -> str ++ show k ++ " : " ++ show v ++ "\n") ""
