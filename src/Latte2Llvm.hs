module Latte2Llvm where

import Parser.AbsLatte

import LatteTypes

import LlvmTypes


------------------------------------------- Latte / LLVM mappings ------------------------------------------------------

mapLatteSimpleType2LlvmType :: SimpleType -> LlvmType
mapLatteSimpleType2LlvmType Int  = TLSingle (TLInteger 32)
mapLatteSimpleType2LlvmType Bool = TLSingle (TLInteger 1)
mapLatteSimpleType2LlvmType Str  = TLSingle (TLPointer (TLSingle (TLInteger 8)))

makePointerToSimpleTypeLlvmType :: SimpleType -> LlvmType
makePointerToSimpleTypeLlvmType = makePointerToType . mapLatteSimpleType2LlvmType

-- TODO I'm not sure if this is ok
-- mapLatteType2LlvmType :: Type -> LlvmType
-- mapLatteType2LlvmType ty =
--     let
--         mapLatteSimpleType2LlvmType :: SimpleType -> LlvmType
--         mapLatteSimpleType2LlvmType Int  = TLSingle (TLInteger 32)
--         mapLatteSimpleType2LlvmType Bool = TLSingle (TLInteger 1)
--         mapLatteSimpleType2LlvmType Str  = TLPointer (TLSingle (TLInteger 8))
--       in
--         case ty of
--             TSimple simpleTy              = mapLatteSimpleType2LlvmType simpleTy
--             TStructured (TArray simpleTy) = TLPointer simpleTy

identAsGlobalName :: Ident -> LlvmIdent
identAsGlobalName (Ident ident) = buildGlobalName ident

identAsLocalName :: Ident -> LlvmIdent
identAsLocalName (Ident ident) = buildLocalName ident


thisPointerLlvmIdent :: LlvmIdent
thisPointerLlvmIdent = buildLocalName thisPointerName
