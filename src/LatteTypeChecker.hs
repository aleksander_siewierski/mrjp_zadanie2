module LatteTypeChecker(checkTypes) where

import Control.Monad.Except
import Control.Monad.Reader

import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.Set as S

import Parser.AbsLatte
import Parser.PrintLatte

import Helpers

import TupleHelpers

import LatteTypes
import LlvmTypes -- TODO to refactorize

import SkelHelpers
import SkelLatte


checkTypes :: Program -> LCMResult (FunctionsMap, ClassesMap)
checkTypes program = runLCM initEnv initLatteState $ transProgram program


data ReturnType =
    NotYet -- means there was, till actually processed statement, no return instruction in function body
  | Partly Type -- means that some branches of code returns given type, but not all, for example after all conditional statements.
  | Fixed Type -- means that for sure this type of return value is returned for code
   deriving (Eq, Ord, Show)


------------------------------------------------ artificial values -----------------------------------------------------

fakeRegisterName :: LlvmIdent
fakeRegisterName = LocalIdent $ IName "FAKE_REG"

fakeValueForTypeWithElemsNo :: Type -> LlvmValue -> LlvmValue
fakeValueForTypeWithElemsNo (TStructured (TArray ty)) sizeVal =
    let
        maybeArrayBody =
            case sizeVal of
                TIntConstant size -> Just (TIntConstant size, fakeRegisterName)
                _                 -> Nothing
      in
        TRegisterArray ty fakeRegisterName maybeArrayBody
fakeValueForTypeWithElemsNo ty _                              =
    TRegisterName ty fakeRegisterName

fakeValueForType :: Type -> LlvmValue
fakeValueForType ty@(TSimple _)                   = fakeValueForTypeWithElemsNo ty $ TIntConstant 0
fakeValueForType (TStructured (TArray ty))        = NotInitialized ty
fakeValueForType ty@(TClass _)                    = TNullPointer ty

fakeLlvmValueForType :: Type -> LlvmValue -- TODO name deprecated
fakeLlvmValueForType = fakeValueForType


---------------------------------------------- type evaluation helpers -------------------------------------------------

typesMatches :: Type -> Type -> LCM ()
typesMatches t1 t2  = do
    match1 <- checkTypeMatches t1 t2
    match2 <- checkTypeMatches t2 t1
    unless (match1 || match2) $ throwError $ "Incompatible types: " ++ printTree t1 ++ " and: " ++ printTree t2
    return ()


returnType :: ReturnType -> ReturnType -> LCM ReturnType
returnType NotYet (Fixed t) = return (Partly t)
returnType NotYet t = return t
returnType t NotYet = returnType NotYet t
returnType (Partly t) rt2 =
    case rt2 of
        Partly t2 -> do
            typesMatches t t2
            return (Partly t2)
        Fixed t2  -> do
            typesMatches t t2
            return (Partly t2)
returnType (Fixed t1) (Fixed t2) = do
    typesMatches t1 t2
    return (Fixed t2)

returnTypeSequence :: ReturnType -> ReturnType -> Stmt -> LCM ReturnType
returnTypeSequence NotYet t _         =
    return t
returnTypeSequence (Fixed t1) t2 stmt =
    throwError $ "Unreachable statement: " ++ printTree stmt ++ "\ntype 1:\n" ++ show t1 ++ "\ntype 2:\n" ++ show t2
returnTypeSequence (Partly t) rt2 _   =
    case rt2 of
        NotYet      -> return (Partly t)
        Partly t2   -> do
            typesMatches t t2
            return (Partly t2)
        Fixed t2    -> do
            typesMatches t t2
            return (Fixed t2)


----------------------------------- helpers for get / set variable types / values --------------------------------------

checkGetVariableM :: Ident -> LCM LlvmValue
checkGetVariableM ident = do
    eitherVarVal <- resolveGetIdentM ident
    case eitherVarVal of
        Right val                     -> return val
        Left (_, _, _, fieldTy) ->
            return $ fakeValueForTypeWithElemsNo fieldTy $ TIntConstant 0

updateIdentTypeM :: Ident -> LlvmValue -> LlvmValue -> LCM LlvmValue
updateIdentTypeM ident (NotInitialized _) newVal = do
    updateIdentValueM ident newVal
    return newVal
updateIdentTypeM ident (TNullPointer _) newVal   = do
    updateIdentValueM ident newVal
    return newVal
updateIdentTypeM _ prevVal _                     =
    return prevVal

abstractCheckSetVariableM :: Ident -> Maybe LlvmValue -> LCM LlvmValue
abstractCheckSetVariableM ident maybeNewVal = do
    eitherVarVal <- resolveSetIdentM ident
    case eitherVarVal of
        Right prevVal               ->
            case maybeNewVal of
                Just newVal ->
                    updateIdentTypeM ident prevVal newVal
                Nothing     ->
                    return prevVal
        Left (_, _, _, fieldTy)     ->
            return $ fakeValueForTypeWithElemsNo fieldTy $ TIntConstant 0

checkSetAndWriteVariableM :: Ident -> LlvmValue -> LCM LlvmValue
checkSetAndWriteVariableM ident newVal =
    abstractCheckSetVariableM ident $ Just newVal

checkSetVariableM :: Ident -> LCM LlvmValue
checkSetVariableM ident        =
    abstractCheckSetVariableM ident Nothing

checkTypeMatches :: Type -> Type -> LCM Bool
checkTypeMatches (TClass cls1) (TClass cls2) =
    check2IsSubclass1 cls1 cls2
checkTypeMatches t1 t2 = return $ t1 == t2

check2IsSubclass1 :: Ident -> Ident -> LCM Bool
check2IsSubclass1 cls1 cls2 =
    if
        cls1 == cls2
      then
        return True
      else do
        cls2ClassBody <- getClassM cls2
        case parentClass cls2ClassBody of
            Nothing         -> return False
            Just cls2Parent -> check2IsSubclass1 cls1 cls2Parent


checkExpressionsTypesMatches :: Expr -> Type -> Expr -> Type -> LcmProcess
checkExpressionsTypesMatches expr0 ty0 expr1 ty1 =
    catchError (checkTypesMatches ty0 ty1)
            (\ e -> throwError $ e ++ "\n\nExpression 1:\n" ++ printTree expr0 ++ "\n\nExpression 2:\n" ++ printTree expr1)

checkVarExprTypesMatches :: Var -> Type -> Expr -> Type -> LcmProcess
checkVarExprTypesMatches var ty0 expr1 ty1 =
    catchError (checkTypesMatches ty0 ty1)
            (\ e -> throwError $ e ++ "\n\nVariable:\n" ++ printTree var ++ "\n\nExpression:\n" ++ printTree expr1)

checkLlvmValueMatches :: Expr -> LatteValue -> Expr -> LatteValue -> LcmProcess
checkLlvmValueMatches expr1 val1 expr2 val2 =
    checkExpressionsTypesMatches expr1 (latteValue2Type val1) expr2 (latteValue2Type val2)

checkTypesMatches :: Type -> Type -> LcmProcess
checkTypesMatches type0 type1 = do
    matches <- checkTypeMatches type0 type1
    unless matches
            (throwError $ "Type mismatches!\nType of argument 1: " ++ show type0 ++ ", type of argument 2: " ++ show type1)


checkLlvmValueHasConcreteType :: LlvmValue  -> Type -> Expr -> LCM ()
checkLlvmValueHasConcreteType exprVal desiredType expr =
  let
    valType = llvmValue2LlvmType exprVal
   in do
    unless (valType == desiredType)
            (throwError $ "Type mismatches!\nArgument type:\n" ++ show exprVal
                ++ "\n\nshould be of type: " ++ show desiredType ++ ", expression:\n\n" ++ printTree expr)
    return ()


--------------------------------------------- arrays related checkers --------------------------------------------------

checkArrayElementAccess :: Ident -> LlvmValue -> LCM SimpleType
checkArrayElementAccess arrayIdent _ = do
    arrayVal <- checkGetVariableM arrayIdent
    case arrayVal of
        NotInitialized _                      ->
            throwError $ "Trying to access element from not initialized aggregate type: " ++ show arrayIdent
        TRegisterArray elemTy _ _             ->
            return elemTy
        _                                     ->
            throwError $ unwords [show arrayIdent, "has no array type, it has type:", show $ latteValue2Type arrayVal]


-------------------------------------------------- general checkers ----------------------------------------------------

checkMainFunctionDefined :: FunctionsMap -> LcmProcess
checkMainFunctionDefined functions =
    let
        (ident, (expectedType, expectedArgs)) = mainFunctionDefinition
      in
        case M.lookup ident functions of
            Nothing        -> throwError "No main function definition!\n"
            Just (t, args) -> do
                typeMatches <- checkTypeMatches expectedType t
                unless typeMatches (
                    throwError $ "main function should return:\n\n" ++ show expectedType ++ "\n\nbut returns:\n\n" ++ show t)
                unless (args == expectedArgs) (
                    throwError $ "main function should have args:\n\n" ++ show expectedArgs ++ "\n\nbut has:\n\n" ++ show args)
                return ()

allowedVariableType :: Type -> LCM Bool
allowedVariableType (TSimple _)              = return True
allowedVariableType (TStructured (TArray _)) = return True
allowedVariableType (TClass ident)           = do
    checkClassType ident
    return True
allowedVariableType _                        = return False

checkVariableType :: Type -> LcmProcess
checkVariableType t = do
    isAllowed <- allowedVariableType t
    unless isAllowed
        (throwError $ "Not allowed argument / variable / return type, has:\n\n" ++ printTree t)


checkArgument :: Arg -> LCM FunctionArg
checkArgument (Arg t ident) = do
    catchError (checkVariableType t) (\ e -> throwError $ e ++ "\n\nfor argument:\n\n" ++ printTree (Arg t ident))
    return (t, ident)

checkAndTransformArgument :: Arg -> LCM FunctionArg
checkAndTransformArgument arg@(Arg t ident@(Ident _)) = do
    checkArgument arg
    return (t, ident)
    -- TODO this prefix doesn't work, I must add prefixes also later in function body
    --return (t, Ident $ ".." ++ idStr) -- adding this prefix to avoid name clashes with class names

checkArgumentTypes :: [(Type, Ident)] -> [(Expr, LlvmValue)] -> LCM ()
checkArgumentTypes argDecls exprVals =
    let
        checkArgsHelper :: FunctionArg -> (Expr, LlvmValue) -> LcmProcess
        checkArgsHelper (argType, argIdent) (expr, val) = do
            unless (isProperRValue val) (throwError $ "Value: " ++ show val ++ " of argument: " ++ printTree expr ++ " is not proper right side value")
            checkExpressionsTypesMatches (EVar argIdent) argType expr $ latteValue2Type val
      in do
        unless (length argDecls == length exprVals) (throwError "Numbers of arguments are different!")
        zipWithM_ checkArgsHelper argDecls exprVals

checkTypeIsValid :: Type -> LCM ()
checkTypeIsValid (TClass ident) = do
    checkClassType ident
    return ()
checkTypeIsValid _              = return ()


------------------------------------------------ class related checkers ------------------------------------------------

checkClassType :: Ident -> LCM ClassBody
checkClassType ident = do
    classesMap <- getClassesMapM
    case M.lookup ident classesMap of
        Nothing     -> throwError $ "For class type: " ++ printTree ident ++ ", this class doesn't exist"
        Just cd     -> return cd

checkClassField :: Ident -> FieldsMap -> Ident -> LCM Type
checkClassField ident classDecl field =
    case M.lookup field classDecl of
        Nothing         -> throwError $ "Class: " ++ printTree ident ++ " has no field: " ++ printTree field
        Just (_, _, ty) -> return ty

checkObjectElementAccess :: Ident -> Ident -> LCM Type
checkObjectElementAccess objIdent field = do
    objVal <- checkGetVariableM objIdent
    case objVal of
        TRegisterName (TClass clsIdent) _ -> do
            classBody <- getClassM clsIdent
            checkClassField clsIdent (classFields classBody) field
        _                                    ->
            throwError $ "Trying to access field: " ++ show field ++ " of non-object variable: " ++ show objIdent ++ " has value: " ++ show objVal

checkProperRValue :: LlvmValue -> Expr -> LCM ()
checkProperRValue val expr =
    unless (isProperRValue val) (throwError $ "Value " ++ show val ++ " of expression: " ++ printTree expr ++ " cannot be used on left side of assignment / return value")


------------------------------------ traversing through code tree functions --------------------------------------------

-- first traversing through definitions
transGetDeclsProgram :: Program -> LCM (FunctionsMap, ClassesMap, Env)
transGetDeclsProgram (Program topdefs) =
    let
        (classDefs, funDefs) = topDefsPartition topdefs
        rawFunDefs = map (\ (FnDef fun) -> fun) funDefs
      in do
        (classesMap, envWithClasses) <- transGetClassDeclsProgram classDefs
        functionPairsList <- local (const envWithClasses) (transGetFunctionDecls (getFunctions envWithClasses) rawFunDefs)
        let
            functionsMap = M.fromList functionPairsList
          in do
            checkMainFunctionDefined functionsMap
            return (functionsMap, classesMap, setFunctions functionsMap envWithClasses)

transGetClassDeclsProgram :: [TopDef] -> LCM (ClassesMap, Env)
transGetClassDeclsProgram classDefs =
    let
        classIdents = map ( \ (TypeDef clsHeader _) -> getClassIdent clsHeader) classDefs
        multipleClassDefs = findDuplicates classIdents
        classesSet = S.fromList classIdents
        -- this initClassMap with empty fields / methods is used to properly validate methods, which use class map when checking types of arguments
        initClassMap = M.fromList $ map (\ ident -> (ident, ClassBody { parentClass = Nothing, classFields = M.empty, classMethods = M.empty })) classIdents
     in do
        env <- ask
        unless (L.null multipleClassDefs) (throwError $ "Multiple class definitions for ids: " ++ show multipleClassDefs)
        envWithClasses <- foldM (\ actEnv classDef -> local (const actEnv) (transGetClassDecl classesSet classDef)) (setClassesMap initClassMap env) classDefs
        return (getClassesMap envWithClasses, envWithClasses)

-- TODO what about classes order?
transGetClassDecl :: S.Set Ident -> TopDef -> LCM Env
transGetClassDecl classesSet (TypeDef classHeader declarations) =
    let
        isField (FVar _ _) = True
        isField _          = False
        (fieldDefs, methodDefs) = L.partition isField declarations
        clsIdent = getClassIdent classHeader
        parentClsIdent = getClassParent classHeader
      in do
        env <- ask
        fieldsMap <- transGetClassFields classesSet clsIdent parentClsIdent fieldDefs
        methodsMap <- transGetClassMethods clsIdent parentClsIdent methodDefs
        let
            newClassesMap = M.insert clsIdent ClassBody { parentClass = parentClsIdent, classFields = fieldsMap, classMethods = methodsMap } $ getClassesMap env
          in
            return $ setClassesMap newClassesMap env


transGetClassFields :: S.Set Ident -> Ident -> Maybe Ident -> [FieldDeclaration] -> LCM FieldsMap
transGetClassFields classesSet clsIdent maybeParentClsIdent fieldDefs = do
    env <- ask
    let
        transGetClassFieldsHelper :: Maybe Ident -> [(Ident, (Int, Ident, Type))]
        transGetClassFieldsHelper Nothing               = []
        transGetClassFieldsHelper (Just parentClsIdent) =
            case getClass parentClsIdent env of
                Just parentClassBody ->
                    M.toList (classFields parentClassBody)
                Nothing              -> []
        parentFields = transGetClassFieldsHelper maybeParentClsIdent
        nextId = L.length parentFields + 1
      in do
        fieldsList <- mapM (transField classesSet clsIdent) $ enumerateListFrom nextId fieldDefs
        let
            allFields = parentFields ++ fieldsList
            multipleFields = findDuplicates (map fst allFields)
            fieldsMap = M.fromList allFields
          in do
            unless (L.null multipleFields) (
                    throwError $ "For class: " ++ printTree clsIdent ++ " there are multiple field names: " ++ show multipleFields)
            return fieldsMap

transGetClassMethods :: Ident -> Maybe Ident -> [FieldDeclaration] -> LCM MethodsMap
transGetClassMethods clsIdent maybeParentClsIdent methodDefs = do
    methodsPairsList <- catchError (transGetFunctionDecls M.empty (map (\ (FMethod funDef) -> funDef) methodDefs)) (
        \ e -> throwError $ e ++" for class: " ++ show clsIdent)
    env <- ask
    let
        changeMethod :: Ident -> (Int, Ident, Ident, FunctionDecl) -> MethodsMap -> MethodsMap
        changeMethod methodId (_, mthName, _, methodDecl) methodsMap =
            let
                previousMethodIdx = sel1 $ methodsMap M.! methodId
              in
                M.insert methodId (previousMethodIdx, mthName, clsIdent, methodDecl) methodsMap
        transGetClassMethodsHelper :: Maybe Ident -> MethodsMap
        transGetClassMethodsHelper Nothing               = M.empty
        transGetClassMethodsHelper (Just parentClsIdent) =
            case getClass parentClsIdent env of
                Just parentClassBody ->
                    classMethods parentClassBody
                Nothing              -> M.empty
        transformToMethodsMap :: [(Ident, FunctionDecl)] -> Int -> MethodsMap
        transformToMethodsMap pairsList startIdx =
            let
                (methodIdents, methodDecls) = unzip pairsList
                methodDeclsWithThisPtr = map (\ (returnTy, args) -> (returnTy, (TClass clsIdent, thisPointerIdent):args)) methodDecls
                methodNames = map (methodName clsIdent) methodIdents
                methodValues = L.zip4 [startIdx..] methodNames (repeat clsIdent) methodDeclsWithThisPtr
                methodsEnumerated = zip methodIdents methodValues
              in
                M.fromList methodsEnumerated
        parentMethods = transGetClassMethodsHelper maybeParentClsIdent
        overwrittenMethodIds = M.keys parentMethods `L.intersect` map fst methodsPairsList
        (overwrittenMethods, newMethods) = L.partition (\ method -> fst method `L.elem` overwrittenMethodIds) methodsPairsList
        changedParentMethods = M.foldWithKey changeMethod parentMethods $ transformToMethodsMap overwrittenMethods 0
        nextId = M.size parentMethods
        newMethodsMap = transformToMethodsMap newMethods nextId
        allMethods = changedParentMethods `M.union` newMethodsMap
      in
        return allMethods

transField :: S.Set Ident -> Ident -> (Int, FieldDeclaration) -> LCM (Ident, (Int, Ident, Type))
transField classesSet clsIdent (fieldIndex, FVar ty ident) = do
    case ty of
        TClass classIdent ->
            unless (S.member classIdent classesSet) (throwError $ "Unknown class name: " ++ show classIdent ++ " for field: " ++ show ident)
        _                 -> return ()
    return (ident, (fieldIndex, clsIdent, ty))

transGetFunctionDecls :: FunctionsMap -> [FunctionDefinition] -> LCM [(Ident, FunctionDecl)]
transGetFunctionDecls initialFunctions functionDefs = do
    programFunctionsList <- mapM transGetFunDecl functionDefs
    let
        allFunctions = M.toList initialFunctions ++ programFunctionsList
        funIds = map fst allFunctions
        multipleFunDefs = findDuplicates funIds
      in do
        unless (L.null multipleFunDefs) (throwError $ "Multiple function / method definitions for ids: " ++ show multipleFunDefs)
        return allFunctions

transGetFunDecl :: FunctionDefinition -> LCM (Ident, FunctionDecl)
transGetFunDecl (Function type' ident args _) = do
    checkTypeIsValid type'
    transformedArgs <- mapM checkAndTransformArgument args
    let
        multipleArgs = findDuplicates $ map snd transformedArgs
      in
        unless (L.null multipleArgs) (throwError $ "Multiple function arguments with same name: " ++ show multipleArgs)
    return (ident, (type', transformedArgs))

transProgram :: Program -> LCM (FunctionsMap, ClassesMap)
transProgram x =
  case x of
    Program topdefs  -> do
      (functionsMap, classesMap, envWithDecls) <- transGetDeclsProgram $ Program topdefs
      local (const envWithDecls) (mapM_ transTopDef topdefs)
      return (functionsMap, classesMap)


-- second traversing through definitions
transTopDef :: TopDef -> LcmProcess
transTopDef (FnDef (Function _ funIdent _ block))   = do
    functionDecl <- getFunctionM funIdent
    transFunctionDefinitionHelper funIdent functionDecl block

transTopDef (TypeDef classHeader fieldDeclarations) =
    let
        clsIdent = getClassIdent classHeader
      in do
        classBody <- getClassM clsIdent
        envWithClassCtx <- setClassContextM clsIdent classBody
        local (const envWithClassCtx) (mapM_ (transFieldDeclaration clsIdent) fieldDeclarations)

transFunctionDefinitionHelper :: Ident -> FunctionDecl  -> Block -> LcmProcess
transFunctionDefinitionHelper ident (retType, args) block = do
    env0 <- ask
    envWithArgs <-
        foldM (\ prevEnv arg -> local (const prevEnv) (transArg arg)) env0 args

    returnTy <- local (const envWithArgs) $ transBlock block
    case returnTy of
      Fixed t2  -> do
          typeMatches <- checkTypeMatches retType t2
          unless typeMatches (
                  throwError $ "Function: " ++ show ident ++ " should return:\n\n" ++ show retType ++ "\n\nbut returns:\n\n" ++ show returnTy)
      Partly t2 -> do
          unless (retType == t2)
                  (throwError $ "Function: " ++ show ident ++ " should return:\n\n" ++ show retType ++ "\n\nbut returns (partly):\n\n" ++ show returnTy)
          unless (retType == Void)
                  (throwError $ "Function: " ++ show ident ++ " not always returns from body, evaluated returnType: " ++ show returnTy)
      _         -> unless (retType == Void)
          (throwError $ "Function: " ++ show ident ++ " not always returns from body, evaluated returnType: " ++ show returnTy)
    return ()

transArg :: FunctionArg -> LCM Env
transArg (ty, ident) =
    let
        argName = getIdentName ident
        regName = buildLocalName argName
        llvmValue =
            case ty of
                TStructured (TArray elemTy) -> TRegisterArray elemTy regName Nothing
                _                           -> TRegisterName ty regName
      in
        addIdentValueM ident llvmValue

transFieldDeclaration :: Ident -> FieldDeclaration -> LcmProcess
transFieldDeclaration clsIdent (FMethod (Function _ method _ block))  = do
    (_, methodFullId, _, (returnTy, functionArgs)) <- getClassMethodM clsIdent method
    transFunctionDefinitionHelper methodFullId (returnTy, (TClass clsIdent, thisPointerIdent):functionArgs) block

transFieldDeclaration _ (FVar _ _)                                    = return ()

transBlock :: Block -> LCM ReturnType
transBlock x = case x of
  Block stmts ->
    let
        varDeclarations = getFirstLevelVarsDeclaration stmts
        multipleVarDecls = findDuplicates varDeclarations
      in do
        unless (L.null multipleVarDecls) (throwError $ "Variable names duplicated! Identifiers:\n\n" ++ show multipleVarDecls)
        env0 <- ask
        (retValue, _) <-
            foldM (\ (retVal, env) stmt ->
                catchError (do
                        (nextRetVal, nextEnv) <- local (const env) (transStmt stmt)
                        joinRetValue <- returnTypeSequence retVal nextRetVal stmt
                        return (joinRetValue, nextEnv))
                    (\ e -> throwError $ unlines [e, "", "in statement:", "", printTree stmt, "Environment:", environmentPrettyPrint env])
            ) (NotYet, env0) stmts
        return retValue

-- TODO could be done better, but no time left. Here I'm checking whether on one block there are same variable name definitions.
getFirstLevelVarsDeclaration :: [Stmt] -> [Ident]
getFirstLevelVarsDeclaration stmts =
    let
        getVarName item =
            case item of
                Init ident _ -> ident
                NoInit ident -> ident
        getFirstLevelVarsDeclarationHelper (Decl _ items) = map getVarName items
        getFirstLevelVarsDeclarationHelper _              = []
      in
        foldl (\ prevIdents stmt -> prevIdents ++ getFirstLevelVarsDeclarationHelper stmt) [] stmts


transStmt :: Stmt -> LCM (ReturnType, Env)
transStmt Empty                                 =
    returnNoVal

transStmt (SExp expr)                           = do
    transExpr expr
    returnNoVal

transStmt (Ret expr)                            = do
    val <- transExpr expr
    checkProperRValue val expr
    let
        lType = latteValue2Type val
      in do
        checkVariableType lType
        returnWithType $ Fixed lType

transStmt VRet                                  =
  returnWithType $ Fixed Void

transStmt (Ass var expr)                        = do
    val <- transExpr expr
    checkProperRValue val expr
    varType <- case var of
        VEntire ident                              -> do
            idValue <- checkSetAndWriteVariableM ident val
            return  $ latteValue2Type idValue
        VComponent (VIndexed arrayIdent indexExpr) -> do
            indexVal <- transExpr indexExpr
            checkLlvmValueHasConcreteType indexVal intType indexExpr
            simpleTy <- checkArrayElementAccess arrayIdent indexVal
            return $ TSimple simpleTy
        VComponent (VField objIdent field)         ->
            checkObjectElementAccess objIdent field
    checkVarExprTypesMatches var varType expr $ latteValue2Type val
    returnNoVal


transStmt (Decl type' items)                    = do
  env0 <- ask
  env1 <- foldM (\ env item -> local (const env) (transItem type' item)) env0 items
  return (NotYet, env1)


transStmt (BStmt block)                         = do
  retValue <- transBlock block
  returnWithType retValue

-- using CondElse to also omit strong checking of return stmt occurrence in case of statements like if (true)
transStmt (Cond expr stmt)                      =
    transStmt (CondElse expr stmt Empty)

-- next two definitions, for ELitTrue and ELitFalse are due to some not understandable requirement, that if (true) {return;} else {} should
-- be treated as correct definition and always returns (which is not true in for example Java)
-- I'm assuming that such deletion of branches in Cond/CondElse is correct is only for simple constant values like true / false and not with more complex.
transStmt (CondElse ELitTrue stmt0 stmt1)       = do
  (stmtVal0, _) <- transStmt stmt0
  transStmt stmt1
  returnWithType stmtVal0

transStmt (CondElse ELitFalse stmt0 stmt1)      = do
  transStmt stmt0
  (stmtVal1, _) <- transStmt stmt1
  returnWithType stmtVal1

transStmt (CondElse expr stmt0 stmt1)           =
    transConditionalStmtsHelper expr stmt0 stmt1

transStmt (While expr stmt) =
    transConditionalStmtHelper expr stmt

transStmt (ForEach t elemIdent arrayIdent stmt) = do
    arrayVal <- checkGetVariableM arrayIdent
    case arrayVal of
        TRegisterArray simpleType _ _ -> do
            checkTypesMatches (TSimple simpleType) t
            envWithElem <- addIdentValueM elemIdent $ fakeLlvmValueForType t
            (retStmt, _) <- local (const envWithElem) $ transStmt stmt
            retType <- returnType retStmt NotYet
            returnWithType retType
        _                             ->
            throwError $ "Identifier " ++ show arrayIdent ++ " should be an array!"


transStmt stmt = -- Inc / Decl statement
  let
    incDecHelper ident = do
      val <- checkSetVariableM ident
      checkLlvmValueHasConcreteType val (TSimple Int) (EVar ident)
      returnNoVal
   in
    case stmt of
      Incr ident  -> incDecHelper ident
      Decr ident  -> incDecHelper ident
      _           -> throwError $ "Unknown statement: " ++ printTree stmt

transItem :: Type -> Item -> LCM Env
transItem t x = do
    catchError (checkVariableType t) (\ e -> throwError $ e ++ " for item: " ++ printTree x)
    (ident, val) <- case x of
        Init ident expr -> do
            val <- transExpr expr
            checkProperRValue val expr
            checkExpressionsTypesMatches (EVar ident) t expr $ latteValue2Type val
            return (ident, val)
        NoInit ident     ->
            return (ident, fakeLlvmValueForType t)
    addIdentValueM ident val


-------------------------------------------- some traversing helpers ---------------------------------------------------

returnWithType :: ReturnType -> LCM (ReturnType, Env) -- no environment change
returnWithType returnTy = do
    env0 <- ask
    return (returnTy, env0)

returnNoVal :: LCM (ReturnType, Env)
returnNoVal = returnWithType NotYet

transConditionalStmtHelper :: Expr -> Stmt -> LCM (ReturnType, Env)
transConditionalStmtHelper conditionExpr stmt0 =
    transConditionalStmtsHelper conditionExpr stmt0 Empty

transConditionalStmtsHelper :: Expr -> Stmt -> Stmt -> LCM (ReturnType, Env)
transConditionalStmtsHelper conditionExpr stmt0 stmt1 = do
    exprVal <- transExpr conditionExpr
    checkLlvmValueHasConcreteType exprVal boolType conditionExpr
    (stmtVal0, _) <- transStmt stmt0
    (stmtVal1, _) <- transStmt stmt1
    retType <- returnType stmtVal0 stmtVal1
    env0 <- ask
    return (retType, env0)


-------------------------------------------------- expressions traversing ----------------------------------------------

transExpr :: Expr -> LCM LlvmValue
transExpr (EApp funId exprs)                                                   = do
  functionDecl <- getFunctionM funId
  functionCallHelper funId functionDecl exprs

transExpr (EString _)                                                          = return $ fakeLlvmValueForType aliasTString

transExpr expr@(ENewArray t sizeExpr)                                          = do
    sizeVal <- transExpr sizeExpr
    checkLlvmValueHasConcreteType sizeVal (TSimple Int) expr
    return $ fakeValueForTypeWithElemsNo (makeArrayType t) sizeVal

transExpr (ENewObject ident)                                                   = do
    checkClassType ident
    return $ fakeValueForTypeWithElemsNo (TClass ident) (TIntConstant 0)

transExpr arrExpr@(EArray ident expr)                                          = do
    indexVal <- transExpr expr
    checkLlvmValueHasConcreteType indexVal (TSimple Int) arrExpr
    elemTy <- checkArrayElementAccess ident indexVal
    return $ fakeValueForType $ TSimple elemTy

transExpr (EFieldAccess ident field@(Ident fieldStr))                          = do
    variableVal <-  checkGetVariableM ident
    case variableVal of
        TRegisterArray _ _ maybeArrayBody          -> do
            unless (fieldStr == "length") (throwError $ "Accessing unknown array field: " ++ fieldStr ++ ", for arrays there is only field 'length'")
            return $ case maybeArrayBody of
                Just (TIntConstant size, _)    -> TIntConstant size
                _                              -> fakeLlvmValueForType intType
        TRegisterName (TClass clsIdent) _          -> do
            classBody <- getClassM clsIdent
            fieldTy <- checkClassField clsIdent (classFields classBody) field
            return $ fakeValueForType fieldTy
        _                                          ->
            throwError $ "Accessing field cannot be used to variable with type: " ++ show variableVal ++ ", variable name: " ++ printTree ident

transExpr (EMethodApp objIdent method exprs)                                   = do
    objVal <- checkGetVariableM objIdent
    case objVal of
        TRegisterName (TClass clsIdent) _ -> do
            (_, functionIdent, _, functionDecl) <- getClassMethodM clsIdent method
            functionCallHelper functionIdent functionDecl (EVar objIdent:exprs)
        _                                 ->
            throwError $ "Calling method on non object variable: " ++ printTree objIdent ++ ", value: " ++ show objVal

transExpr (ENullPtr ident)                                                     =
    return $ fakeValueForType (TClass ident)

transExpr (ELitInt n)                                                          = do
    unless (n <= latteMaxInt) (throwError $ "Integer too big: " ++ show n)
    return $ TIntConstant $ fromIntegral n

transExpr ELitTrue                                                             = return $ fakeLlvmValueForType (TSimple Bool)
transExpr ELitFalse                                                            = return $ fakeLlvmValueForType (TSimple Bool)
transExpr (Not expr)                                                           = do
  evExpr <- transExpr expr
  checkLlvmValueHasConcreteType evExpr (TSimple Bool) (Not expr)
  return evExpr

transExpr (Neg expr)                                                           = do
  evExpr <- transExpr expr
  checkLlvmValueHasConcreteType evExpr (TSimple Int) (Neg expr)
  return $ case evExpr of
      TIntConstant n  -> TIntConstant (-n)
      _               -> evExpr

transExpr (EVar ident)                                                         =
    checkGetVariableM ident

transExpr (EMul expr0 mulop expr)                                              = do
  expr0val <- transExpr expr0
  exprVal  <- transExpr expr
  checkLlvmValueHasConcreteType expr0val (TSimple Int) $ EMul expr0 mulop expr
  checkLlvmValueHasConcreteType exprVal (TSimple Int) $ EMul expr0 mulop expr
  return expr0val

transExpr (EAdd expr0 Minus expr)                                              = do -- TODO code repeat, introduce helper method
  expr0val <- transExpr expr0
  exprVal  <- transExpr expr
  checkLlvmValueHasConcreteType expr0val (TSimple Int) $ EAdd expr0 Minus expr
  checkLlvmValueHasConcreteType exprVal (TSimple Int) $ EAdd expr0 Minus expr
  return expr0val

transExpr addExpr@(EAdd expr0 Plus expr)                                       = do
    expr0val <- transExpr expr0
    exprVal  <- transExpr expr
    case llvmValue2LlvmType expr0val of
        TSimple Int       -> do
            checkLlvmValueHasConcreteType expr0val intType addExpr
            checkLlvmValueHasConcreteType exprVal intType addExpr
            return expr0val
        TSimple Str       -> do
            checkLlvmValueHasConcreteType expr0val aliasTString addExpr
            checkLlvmValueHasConcreteType exprVal aliasTString addExpr
            return expr0val

transExpr (ERel expr0 _ expr)                                                  = do
  expr0val <- transExpr expr0
  exprVal  <- transExpr expr
  checkLlvmValueMatches expr0 expr0val expr exprVal
  return $ fakeLlvmValueForType boolType

transExpr addExpr@(EAnd expr0 expr)                                            = do
  expr0val <- transExpr expr0
  exprVal  <- transExpr expr
  checkLlvmValueHasConcreteType expr0val boolType addExpr
  checkLlvmValueHasConcreteType exprVal boolType addExpr
  return expr0val

transExpr orExpr@(EOr expr0 expr)                                              = do
  expr0val <- transExpr expr0
  exprVal  <- transExpr expr
  checkLlvmValueHasConcreteType expr0val (TSimple Bool) orExpr
  checkLlvmValueHasConcreteType exprVal (TSimple Bool) orExpr
  return expr0val


functionCallHelper :: Ident -> FunctionDecl -> [Expr] -> LCM LlvmValue
functionCallHelper funId functionDecl@(returnTy, args) exprs = do
    exprWithVals <- mapM ( \ e -> do
            val <- transExpr e
            return (e, val)) exprs
    catchError (checkArgumentTypes args exprWithVals) (\ e ->
            throwError $ unlines [ e, "Function:", printTree funId, "", "Type:", "", show functionDecl, "",
                    "Given args:", "", commaJoinList $ map (\ (ex, v) -> printTree ex ++ ": " ++ show v) exprWithVals])
    return $ fakeValueForTypeWithElemsNo returnTy $ fakeValueForType intType
