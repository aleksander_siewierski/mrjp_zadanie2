{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleInstances #-}

module TupleHelpers where


class ApplyElem1 container elem | container -> elem where
    sel1 :: container -> elem
    upd1 :: elem -> container -> container

class ApplyElem2 container elem | container -> elem where
    sel2 :: container -> elem
    upd2 :: elem -> container -> container

class ApplyElem3 container elem | container -> elem where
    sel3 :: container -> elem
    upd3 :: elem -> container -> container

class ApplyElem4 container elem | container -> elem where
    sel4 :: container -> elem
    upd4 :: elem -> container -> container

class ApplyElem5 container elem | container -> elem where
    sel5 :: container -> elem
    upd5 :: elem -> container -> container

class ApplyElem6 container elem | container -> elem where
    sel6 :: container -> elem
    upd6 :: elem -> container -> container


instance ApplyElem1 (a, b) a where
    sel1 (e, _) = e
    upd1 a2 (_, b) = (a2, b)

instance ApplyElem1 (a, b, c) a where
    sel1 (e, _, _) = e
    upd1 a2 (_, b, c) = (a2, b, c)

instance ApplyElem1 (a, b, c, d) a where
    sel1 (e, _, _, _) = e
    upd1 a2 (_, b, c, d) = (a2, b, c, d)

instance ApplyElem1 (a, b, c, d, e) a where
    sel1 (e, _, _, _, _) = e
    upd1 a2 (_, b, c, d, e) = (a2, b, c, d, e)

instance ApplyElem1 (a, b, c, d, e, f) a where
    sel1 (e, _, _, _, _, _) = e
    upd1 a2 (_, b, c, d, e, f) = (a2, b, c, d, e, f)


instance ApplyElem2 (a, b) b where
    sel2 (_, e) = e
    upd2 b2 (a, _) = (a, b2)

instance ApplyElem2 (a, b, c) b where
    sel2 (_, e, _) = e
    upd2 b2 (a, _, c) = (a, b2, c)

instance ApplyElem2 (a, b, c, d) b where
    sel2 (_, e, _, _) = e
    upd2 b2 (a, _, c, d) = (a, b2, c, d)

instance ApplyElem2 (a, b, c, d, e) b where
    sel2 (_, e, _, _, _) = e
    upd2 b2 (a, _, c, d, e) = (a, b2, c, d, e)

instance ApplyElem2 (a, b, c, d, e, f) b where
    sel2 (_, e, _, _, _, _) = e
    upd2 b2 (a, _, c, d, e, f) = (a, b2, c, d, e, f)


instance ApplyElem3 (a, b, c) c where
    sel3 (_, _, e) = e
    upd3 c2 (a, b, _) = (a, b, c2)

instance ApplyElem3 (a, b, c, d) c where
    sel3 (_, _, e, _) = e
    upd3 c2 (a, b, _, d) = (a, b, c2, d)

instance ApplyElem3 (a, b, c, d, e) c where
    sel3 (_, _, e, _, _) = e
    upd3 c2 (a, b, _, d, e) = (a, b, c2, d, e)

instance ApplyElem3 (a, b, c, d, e, f) c where
    sel3 (_, _, e, _, _, _) = e
    upd3 c2 (a, b, _, d, e, f) = (a, b, c2, d, e, f)


instance ApplyElem4 (a, b, c, d) d where
    sel4 (_, _, _, e) = e
    upd4 d2 (a, b, c, _) = (a, b, c, d2)

instance ApplyElem4 (a, b, c, d, e) d where
    sel4 (_, _, _, e, _) = e
    upd4 d2 (a, b, c, _, e) = (a, b, c, d2, e)

instance ApplyElem4 (a, b, c, d, e, f) d where
    sel4 (_, _, _, e, _, _) = e
    upd4 d2 (a, b, c, _, e, f) = (a, b, c, d2, e, f)


instance ApplyElem5 (a, b, c, d, e) e where
    sel5 (_, _, _, _, e) = e
    upd5 e2 (a, b, c, d, _) = (a, b, c, d, e2)

instance ApplyElem5 (a, b, c, d, e, f) e where
    sel5 (_, _, _, _, e, _) = e
    upd5 e2 (a, b, c, d, _, f) = (a, b, c, d, e2, f)


instance ApplyElem6 (a, b, c, d, e, f) f where
    sel6 (_, _, _, _, _, e) = e
    upd6 f2 (a, b, c, d, e, _) = (a, b, c, d, e, f2)
