module LlvmEmiter where

import qualified Data.Map as M

import Parser.AbsLatte as Latte

import Helpers

import LatteTypes
import LlvmTypes
import Latte2Llvm

import AbstractEmiter


libraryDeclarations :: [String]
libraryDeclarations = [
    "declare void @printInt(i32)",
    "declare void @printString(i8*)",
    "declare void @error()",
    "declare i32 @readInt()",
    "declare i8* @readString()",
    "declare noalias i8* @malloc(i32) nounwind",
    "declare void @free(i8*) nounwind",
    "declare i32 @strlen(i8* nocapture) nounwind readonly",
    "declare i8* @strcat(i8*, i8* nocapture) nounwind",
    "declare i8* @strcpy(i8*, i8* nocapture) nounwind"
  ]


emitProgramPrologue :: [GlobalStringVariable] -> [String]
emitProgramPrologue constDeclarations =
    buildConstDeclarations constDeclarations ++
    [emptyline] ++
    libraryDeclarations


--------------------------------------- various general helper emitters ------------------------------------------------

emitSeparator :: String
emitSeparator = ", "

makeList :: [String] -> String
makeList = joinList emitSeparator

emitIdent :: Ident -> String
emitIdent (Ident s) = "%" ++ s


------------------------------------- type / constant / register name emitters -----------------------------------------

emitNullPtr :: String
emitNullPtr = "null"

emitPointer :: String
emitPointer = "*"

simpleTypeSizeOf :: SimpleType -> Int
simpleTypeSizeOf Int  = 4
simpleTypeSizeOf Bool = 1
simpleTypeSizeOf Str  = 8


typeSizeOf :: Type -> Int
typeSizeOf (TSimple sty) = simpleTypeSizeOf sty
typeSizeOf (TClass _)    = 8 -- TODO for sure?
-- TODO to be continued...

classSizeOf :: FieldsMap -> Int
classSizeOf = M.fold (\ (_, _, ty) acc -> acc + typeSizeOf ty) 0

emitLatteSimpleType :: SimpleType -> String
emitLatteSimpleType Bool = "i1"
emitLatteSimpleType Int  = "i32"
emitLatteSimpleType Str  = "i8" ++ emitPointer

emitGenericLatteType :: Type -> String
emitGenericLatteType (TSimple t)              = emitLatteSimpleType t
emitGenericLatteType (TStructured (TArray t)) = "{ i32, [0 x " ++ emitLatteSimpleType t ++ "] }"
emitGenericLatteType (Void)                   = "void"
emitGenericLatteType (TClass ident)           = emitIdent ident ++ emitPointer
emitGenericLatteType (Fun returnTy args)      = emitLatteType returnTy ++ "(" ++ makeList (map emitLatteType args) ++ ")" ++ emitPointer

emitRawLatteType :: Type -> String
emitRawLatteType = emitGenericLatteType

emitLatteType :: Type -> String
emitLatteType ty@(TStructured _) = emitRawLatteType ty ++ emitPointer
emitLatteType ty                 = emitGenericLatteType ty


emitTypePointer :: Type -> String
emitTypePointer ty = emitLatteType ty ++ emitPointer

abstractEmitLatteTypeValuePair :: Type -> String -> String
abstractEmitLatteTypeValuePair t valueStr = emitLatteType t ++ " " ++ valueStr

emitLatteTypeValuePair :: Type -> LlvmValue -> String
emitLatteTypeValuePair t value = abstractEmitLatteTypeValuePair t $ emitLlvmValue value

emitTypeRnPair :: Type -> LlvmIdent -> String
emitTypeRnPair ty rn = abstractEmitLatteTypeValuePair ty $ show rn

emitLlvmValuePair :: LlvmValue -> String
emitLlvmValuePair TVoidConstant                           = emitLatteType Void
emitLlvmValuePair v                                       = emitLatteTypeValuePair (latteValue2Type v) v

emitLlvmValue :: LlvmValue -> String
emitLlvmValue (TIntConstant  n)           = show n
emitLlvmValue (TBoolConstant b)           = emitBoolean b
emitLlvmValue (TRegisterName _ name)      = show name
emitLlvmValue (TRegisterString regName _) = show regName
emitLlvmValue (TNullPointer _)            = emitNullPtr
emitLlvmValue (TRegisterArray _ rn _)     = show rn

emitPointerLlvmValuePair :: LlvmValue -> String
emitPointerLlvmValuePair (TRegisterName ty rn)             =
    emitTypePointer ty ++ " " ++ show rn
emitPointerLlvmValuePair arrTy@(TRegisterArray _ arrRn _)  =
    emitTypePointer (latteValue2Type arrTy) ++ " " ++ show arrRn

emitBoolean :: Bool -> String
emitBoolean True  = "true"
emitBoolean False = "false"

charLlvmType :: LlvmType
charLlvmType = TLSingle (TLInteger 8)

intLlvmType :: LlvmType
intLlvmType = TLSingle (TLInteger 32)

stringLlvmType :: Int -> LlvmType
stringLlvmType size = TLAggregate (TLArray charLlvmType size)

stringPointerLlvmType :: LlvmType
stringPointerLlvmType = TLSingle (TLPointer charLlvmType)

pointerLlvmType :: LlvmType -> LlvmType
pointerLlvmType llvmTy = TLSingle (TLPointer llvmTy)

emitFunctionType :: FunctionDecl -> String
emitFunctionType = emitLatteType . function2Type


-------------------------------------------------- casting emitters ----------------------------------------------------

emitBitcastByLatteType :: Type -> LlvmIdent -> Type -> LlvmIdent -> [String]
emitBitcastByLatteType fromTy rn castToType resultRn = [
    emitInstructionWithResultRn resultRn $ "bitcast " ++ emitLatteType fromTy ++ " " ++ show rn ++ " to " ++ emitLatteType castToType -- TODO ugly
  ]

emitBitcast :: LlvmValue -> LlvmType -> LlvmIdent -> [String]
emitBitcast valRn castToType resultRn = [
    emitInstructionWithResultRn resultRn $ "bitcast " ++ emitPointerLlvmValuePair valRn ++ " to " ++ show castToType ++ "*" -- TODO ugly
  ]

emitBitcastByLlvmType :: LlvmIdent -> LlvmType -> LlvmType -> LlvmIdent -> [String]
emitBitcastByLlvmType identRn identTy castToType resultRn = [
    emitInstructionWithResultRn resultRn $ "bitcast " ++ show identTy ++ " " ++ show identRn ++ " to " ++ show castToType -- TODO ugly
  ]

----------------------------------------------- basic instruction emitters ---------------------------------------------

emitInstruction :: String -> String
emitInstruction instruction =
    indent ++ instruction

emitInstructionWithResultRn :: LlvmIdent -> String -> String
emitInstructionWithResultRn resultRn instruction =
    emitInstruction $ show resultRn ++ " = " ++ instruction


emitStmtOnStrings :: LlvmIdent -> String -> Type -> String -> String -> String
emitStmtOnStrings resultRn stmt ty arg0 arg1 =
    emitInstructionWithResultRn resultRn $ stmt ++ " " ++ emitLatteType ty ++ " " ++ arg0 ++ emitSeparator ++ arg1


emitStmtOnRegNames :: LlvmIdent -> String -> Type -> LlvmIdent -> LlvmIdent -> String
emitStmtOnRegNames resultRn stmt ty reg0 reg1 =
    emitStmtOnStrings resultRn stmt ty (show reg0) (show reg1)

emitStmtFirstRegName :: LlvmIdent -> String -> Type -> LlvmIdent -> LlvmValue -> String
emitStmtFirstRegName resultRn stmt ty reg0 val1 =
    emitStmtOnStrings resultRn stmt ty (show reg0) (emitLlvmValue val1)

emitStmtSecondRegName :: LlvmIdent -> String -> Type -> LlvmValue -> LlvmIdent -> String
emitStmtSecondRegName resultRn stmt ty val0 reg1 =
    emitStmtOnStrings resultRn stmt ty (emitLlvmValue val0) (show reg1)

emitStmtWithRegName :: LlvmIdent -> String ->  Type -> LlvmValue -> LlvmValue -> String
emitStmtWithRegName resultRn stmt lType val0 val1 =
    emitStmtOnStrings resultRn stmt lType (emitLlvmValue val0) (emitLlvmValue val1)



----------------------------------------- all memory related emiters ---------------------------------------------------

emitMalloc :: LlvmIdent -> LlvmValue -> [String]
emitMalloc resultRn bytesNoVal = [
    emitInstructionWithResultRn resultRn "call i8* @malloc(i32 " ++ emitLlvmValue bytesNoVal ++ ")"
  ]

emitAlloca :: LlvmIdent -> Type -> [String]
emitAlloca regName ty = emitAllocaNumElements regName ty (TIntConstant 0)

emitAllocaNumElements :: LlvmIdent -> Type -> LlvmValue -> [String]
emitAllocaNumElements regName ty elementCountVal =
    let
        numElementsPartStr =
            case elementCountVal of
                TIntConstant 0 -> ""
                _              -> emitSeparator ++ emitLlvmValuePair elementCountVal

      in [
        emitInstructionWithResultRn regName $ "alloca " ++ emitRawLatteType ty ++ numElementsPartStr
      ]

emitLoad :: LlvmIdent -> Type -> LlvmIdent -> [String]
emitLoad ptrRn ty loadedRn = [
    emitInstructionWithResultRn loadedRn $ "load " ++ emitTypePointer ty ++ " " ++ show ptrRn
  ]

emitLoadByLlvmType :: LlvmIdent -> LlvmType -> LlvmIdent -> [String]
emitLoadByLlvmType ptrRn ty loadedRn = [
    emitInstructionWithResultRn loadedRn $ "load " ++ show ty ++ "* " ++ show ptrRn -- TODO ugly pointer usage
  ]

emitStoreSimple :: LlvmIdent -> LlvmValue -> [String] -- TODO refactorize all emitStore* functions
emitStoreSimple ptrRn value =
    emitStore ptrRn (latteValue2Type value) value

emitStore :: LlvmIdent -> Type -> LlvmValue -> [String] -- REMARK here type is not pointer, as it's Latte type
emitStore ptrRn ty value = [
    emitInstruction $ "store " ++ emitLlvmValuePair value ++ emitSeparator ++ emitTypePointer ty ++ " " ++ show ptrRn
  ]

emitStoreByRn :: LlvmIdent -> Type -> LlvmIdent -> [String] -- REMARK here type is not pointer, as it's Latte type
emitStoreByRn ptrRn ty valueRn = [
    emitInstruction $ "store " ++ emitLatteType ty ++ " " ++ show valueRn ++ emitSeparator ++ emitTypePointer ty ++ " " ++ show ptrRn
  ]

emitStoreByLlvmType :: LlvmIdent -> LlvmIdent -> LlvmType -> [String]
emitStoreByLlvmType ptrRn valueRn valueLType = [
    emitInstruction $ "store " ++ show valueLType ++ " " ++ show valueRn ++ emitSeparator ++ show valueLType ++ "* " ++ show ptrRn
  ]

emitInsertValue :: Type -> LlvmIdent -> LlvmValue -> Int -> LlvmIdent -> [String]
emitInsertValue structTy structRn value index newStructRn = [
    emitInstructionWithResultRn newStructRn $ "insertvalue " ++ emitTypeRnPair structTy structRn ++ emitSeparator
            ++ emitLlvmValuePair value ++ emitSeparator ++ show index
  ]

emitInsertLlvmTypedValue :: Type -> LlvmIdent -> LlvmType -> LlvmIdent -> Int -> LlvmIdent -> [String]
emitInsertLlvmTypedValue structTy structRn valueLlvmType valueRn index newStructRn = [
    emitInstructionWithResultRn newStructRn $ "insertvalue " ++ emitTypeRnPair structTy structRn ++ emitSeparator
            ++ show valueLlvmType ++ " " ++ show valueRn ++ emitSeparator ++ show index
  ]

emitExtractValue :: Type -> LlvmIdent -> Int -> LlvmIdent -> [String]
emitExtractValue structTy structRn index newStructRn = [
    emitInstructionWithResultRn newStructRn $ "extractvalue " ++ emitTypeRnPair structTy structRn ++ emitSeparator
            ++ show index
  ]

emitAbstractGetElementPtr :: LlvmIdent -> String -> [LlvmValue] -> LlvmIdent -> String
emitAbstractGetElementPtr aggregatePtrRn typeStr indicesVal elementPtrRn =
    emitInstructionWithResultRn elementPtrRn $ "getelementptr " ++ typeStr ++ " " ++ show aggregatePtrRn
            ++ emitSeparator ++ makeList (map emitLlvmValuePair indicesVal)

emitGetElementPtr :: LlvmIdent -> Type -> [LlvmValue] -> LlvmIdent -> String
emitGetElementPtr aggregatePtrRn ty =
    emitAbstractGetElementPtr aggregatePtrRn (emitLatteType ty)

emitGetElementPtrByPtr :: LlvmIdent -> Type -> [LlvmValue] -> LlvmIdent -> String
emitGetElementPtrByPtr aggregatePtrRn ty =
    emitAbstractGetElementPtr aggregatePtrRn (emitTypePointer ty)

emitGetLlvmTypeElementPtr :: LlvmIdent -> LlvmType -> [LlvmValue] -> LlvmIdent -> String
emitGetLlvmTypeElementPtr aggregatePtrRn llvmType =
    emitAbstractGetElementPtr aggregatePtrRn (show llvmType)


------------------------------------------- string related emitters ----------------------------------------------------

emitConcatStrings :: LlvmIdent -> LlvmIdent -> LlvmValue -> LlvmIdent -> Int -> [String]
emitConcatStrings strRegName1 strRegName2 concatStrLenVal concatStrRegName startIndex =
  let
    mallocRegName = buildLocalVariable startIndex
    strCpyRegName = buildLocalVariable (startIndex + 1)
   in [
    emitFunCallWithRegisterName mallocRegName (Ident "malloc") (TSimple Str) [concatStrLenVal],
    emitFunCallWithRegisterName strCpyRegName (Ident "strcpy") (TSimple Str) [buildRegValue aliasTString mallocRegName, buildRegValue aliasTString strRegName1],
    emitFunCallWithRegisterName concatStrRegName (Ident "strcat") (TSimple Str) [buildRegValue aliasTString strCpyRegName, buildRegValue aliasTString strRegName2]
   ]

emitCountBothStringLength :: LlvmIdent -> LlvmIdent -> LlvmIdent -> Int -> [String]
emitCountBothStringLength strRegId1 strRegId2 concatStrLenRegId freeRegIndex =
  let
    reg1 = buildLocalVariable freeRegIndex
    reg2 = buildLocalVariable $ freeRegIndex + 1
    reg3 = buildLocalVariable $ freeRegIndex + 2
   in [
    emitFunCallWithRegisterName reg1 (Ident "strlen") (TSimple Int) [buildRegValue aliasTString strRegId1],
    emitFunCallWithRegisterName reg2 (Ident "strlen") (TSimple Int) [buildRegValue aliasTString strRegId2],
    emitStmtWithRegName reg3 "add" (TSimple Int) (buildRegValue (TSimple Int) reg1) (TIntConstant 1),
    emitStmtWithRegName concatStrLenRegId "add" (TSimple Int) (buildRegValue (TSimple Int) reg3) (buildRegValue (TSimple Int) reg2)
   ]

emitCountOneStringLength :: LlvmIdent -> Int -> LlvmIdent -> Int -> [String]
emitCountOneStringLength strRegId1 str2Len concatStrLenRegId freeRegIndex =
    let
        tmpRn = buildLocalVariable freeRegIndex
      in [
        emitFunCallWithRegisterName tmpRn (Ident "strlen") (TSimple Int) [buildRegValue aliasTString strRegId1],
        emitStmtWithRegName concatStrLenRegId "add" (TSimple Int) (buildRegValue aliasTString tmpRn) (TIntConstant $ str2Len + 1)
      ]

emitGetStringElementPtr :: LlvmIdent -> GlobalStringVariable -> String
emitGetStringElementPtr ptrRn (GlobalStringVariable name _ len) =
    let
        indices = replicate 2 (TIntConstant 0)
        strType = TLAggregate (TLArray (TLSingle (TLInteger 8)) len)
      in
        emitGetLlvmTypeElementPtr name strType indices ptrRn


buildConstDeclarations :: [GlobalStringVariable] -> [String]
buildConstDeclarations = map buildConstDeclaration

buildConstDeclaration :: GlobalStringVariable -> String
buildConstDeclaration (GlobalStringVariable name str strLen) =
    let
        escapedStr = getEscapedString str
        typeStr = "[" ++ show strLen ++ " x i8] " -- TODO ugly
      in
        show name ++ " = private constant " ++ typeStr ++ " c\"" ++ escapedStr ++ "\\00\""


-------------------------------------------- array related emitters ----------------------------------------------------

emitGetArrayElementPtr :: LlvmIdent -> SimpleType -> LlvmValue -> LlvmIdent -> [String]
emitGetArrayElementPtr arrayElemsRn simpleType indexVal elemPtrRn = [
    emitInstructionWithResultRn elemPtrRn ("getelementptr " ++ emitLatteSimpleType simpleType ++ "* "
            ++ show arrayElemsRn ++ ", i32 " ++ emitLlvmValue indexVal)
  ]

emitGetArrayElement :: LlvmIdent -> SimpleType -> LlvmIdent -> [String]
emitGetArrayElement elemPtrRn simpleType elemRegName = [
    emitInstructionWithResultRn elemRegName $ "load " ++ emitLatteSimpleType simpleType ++ "* " ++ show elemPtrRn
  ]

emitSetArrayElement :: LlvmIdent -> SimpleType -> LlvmValue -> [String]
emitSetArrayElement elemPtrRn simpleType value = [
    emitInstruction $ "store " ++ emitLlvmValuePair value ++ emitSeparator ++ emitLatteSimpleType simpleType ++ "* " ++ show elemPtrRn
  ]


------------------------------------------ function related emitters ----------------------------------------------------

abstractEmitFunCallHelper :: LlvmIdent -> Type -> [LlvmValue] -> String
abstractEmitFunCallHelper functionIdent returnType args =
    "call " ++ emitLatteType returnType ++ " " ++ show functionIdent ++ "(" ++ makeList (map emitLlvmValuePair args) ++  ")"

emitFunCall :: Ident -> Latte.Type -> [LlvmValue] -> String
emitFunCall ident returnType args =
    emitInstruction $ abstractEmitFunCallHelper (identAsGlobalName ident) returnType args

emitFunCallWithRegisterName :: LlvmIdent -> Ident -> Latte.Type -> [LlvmValue] -> String
emitFunCallWithRegisterName registerName ident returnType args =
    emitInstructionWithResultRn registerName $ abstractEmitFunCallHelper (identAsGlobalName ident) returnType args

emitFunCallByLlvmIdent :: LlvmIdent -> Type -> [LlvmValue] -> String
emitFunCallByLlvmIdent ident returnType args =
    emitInstruction $ abstractEmitFunCallHelper ident returnType args

emitFunCallByLlvmIdentWithRegisterName :: LlvmIdent -> LlvmIdent -> Latte.Type -> [LlvmValue] -> String
emitFunCallByLlvmIdentWithRegisterName registerName ident returnType args =
    emitInstructionWithResultRn registerName $ abstractEmitFunCallHelper ident returnType args

emitFunctionSignature :: (Type, Ident) -> [FunctionArg] -> [String]
emitFunctionSignature (t, Ident name) args =
  let
    argsStr = makeList $ map (\(ty, Ident argName) -> emitArg ty $ buildLocalName argName) args
   in [
    emptyline,
    "define " ++ emitTypeRnPair t (buildGlobalName name) ++  "(" ++ argsStr ++ ") nounwind {"
  ]

emitArg :: Type -> LlvmIdent -> String
emitArg =
    emitTypeRnPair

emitFunctionEnd :: String
emitFunctionEnd = "}"

emitReturn :: LlvmValue -> String
emitReturn val =
  indent ++ "ret " ++ emitLlvmValuePair val

emitVoidReturn :: String
emitVoidReturn =
  emitReturn TVoidConstant


---------------------------------------------- expression helper emitters ----------------------------------------------

emitEvaluateOppositeNumber :: LlvmValue -> LlvmIdent -> LlvmIdent -> [String]
emitEvaluateOppositeNumber llvmValue oppositeRegName freeRegName = [
    emitStmtWithRegName freeRegName "sub" (TSimple Int) llvmValue llvmValue,
    emitStmtWithRegName oppositeRegName "sub" (TSimple Int) (TRegisterName (TSimple Int) freeRegName) llvmValue
  ]

emitEvaluateNegation :: LlvmValue -> LlvmIdent -> [String]
emitEvaluateNegation llvmVal negationRegName = [
  emitStmtWithRegName negationRegName "xor" (TSimple Bool) llvmVal (TBoolConstant True)
 ]

emitUnconditionalBr :: LlvmIdent -> String
emitUnconditionalBr label =
  indent ++ "br label " ++ show label

emitConditionalBr :: LlvmIdent -> LlvmIdent -> LlvmIdent -> String
emitConditionalBr regName ifTrueLabel ifFalseLabel =
  indent ++ "br " ++ emitLatteType (TSimple Bool) ++ " " ++ show regName ++ ", label " ++ show ifTrueLabel ++ ", label " ++ show ifFalseLabel

emitLabel :: LlvmIdent -> [String]
emitLabel (LocalIdent name) = [
  "",
  show name ++ ":" ] -- label header without "%"


emitPhiNode :: LlvmIdent -> Type -> [(LlvmIdent, LlvmValue)] -> String
emitPhiNode phiRegName t phiBlocks =
  indent ++ show phiRegName ++ " = phi " ++ emitLatteType t ++ " " ++ makeList (map emitPhiBlock phiBlocks)

emitPhiBlock :: (LlvmIdent, LlvmValue) -> String
emitPhiBlock (labelName, llvmValue) =
  "[" ++ emitLlvmValue llvmValue ++ ", " ++ show labelName ++ "]"


---------------------------------------------- classes related emitters ------------------------------------------------

emitStructureType :: Ident -> [Type] -> [String]
emitStructureType name fieldTypes = [
    "",
    emitIdent name ++ " = type { " ++ makeList (map emitLatteType fieldTypes) ++ " }"
  ]

emitClassVTableType :: Ident -> [FunctionDecl] -> [String]
emitClassVTableType vtableIdent methodsList = [
    "",
    emitIdent vtableIdent ++ " = type { " ++ makeList (map emitFunctionType methodsList) ++ " }"
  ]

emitClassVTableData :: LlvmIdent -> Ident -> [(Int, Ident, Ident, FunctionDecl)] -> [String]
emitClassVTableData vTableRn vTableType methods = [
    "",
    show vTableRn ++ " = global " ++ emitIdent vTableType ++ " {",
    joinList (emitSeparator ++ "\n") $ map (\ (_, methodId, _, methodDecl) -> indent ++ emitFunctionType methodDecl ++ " " ++ show (identAsGlobalName methodId)) methods,
    "}"
  ]
