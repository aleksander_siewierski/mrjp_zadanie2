@dnl           = internal constant [4 x i8] c"%d\0A\00"
@d             = internal constant [3 x i8] c"%d\00"
@s             = internal constant [3 x i8] c"%s\00"
@runtime_error = internal constant [14 x i8] c"runtime error\00"


declare i32 @printf(i8*, ...)
declare i32 @scanf(i8*, ...)
declare i32 @puts(i8*)

declare noalias i8* @malloc(i32) nounwind
declare i32 @strlen(i8* nocapture) nounwind readonly
declare i8* @strcat(i8*, i8* nocapture) nounwind
declare i8* @strcpy(i8*, i8* nocapture) nounwind
declare void @exit(i32) nounwind

define void @printInt(i32 %x) {
entry: %t0 = getelementptr [4 x i8]* @dnl, i32 0, i32 0
    call i32 (i8*, ...)* @printf(i8* %t0, i32 %x)
    ret void
}

define void @printString(i8* %s) {
entry:  call i32 @puts(i8* %s)
    ret void
}

define i32 @readInt() {
entry:  %res = alloca i32
        %t1 = getelementptr [3 x i8]* @d, i32 0, i32 0
    call i32 (i8*, ...)* @scanf(i8* %t1, i32* %res)
    %t2 = load i32* %res
    ret i32 %t2
}

define i8* @readString() {
entry:  %tmp = call i8* @malloc(i32 256) ; TODO
        %t1 = getelementptr [3 x i8]* @s, i32 0, i32 0
    call i32 (i8*, ...)* @scanf(i8* %t1, i8* %tmp)
    %s1 = call i32 @strlen(i8* %tmp)
    %s2 = add i32 %s1, 1
    %res = call i8* @malloc(i32 %s2)
    call i8* @strcpy(i8* %res, i8* %tmp)
    ret i8* %res
}

define void @error() {
entry: %t1 = getelementptr [14 x i8]* @runtime_error, i32 0, i32 0
    call void @printString(i8* %t1)
    call void @exit(i32 -1)
    ret void
}
