#/bin/bash

. tests_commons.sh

BAD_TESTS_DIR=$MAIN_TESTS/bad

ADDITIONAL_BAD_TESTS=$ADDITIONAL_TESTS/bad
ADDITIONAL_TESTS_1=$ADDITIONAL_BAD_TESTS/infinite_loop
ADDITIONAL_TESTS_2=$ADDITIONAL_BAD_TESTS/runtime
ADDITIONAL_TESTS_3=$ADDITIONAL_BAD_TESTS/semantic
MY_BAD_TESTS=$MY_TESTS_DIR/bad

ALL_TESTS_DIRS=($BAD_TESTS_DIR $ADDITIONAL_TESTS_1, $ADDITIONAL_TESTS_2, $ADDITIONAL_TESTS_3, $MY_BAD_TESTS)


function perform_test {
    SOURCE_FILE=$1
    VERBOSE=$2

    SOURCE_FILENAME=${SOURCE_FILE##*/} # only name of file, without path
    LLVM_BITCODE=${SOURCE_FILE%.*}.bc

    CORRECT_OUTPUT_FILE=${SOURCE_FILE%.*}.output

    LATC_OUTPUT=`./latc -v $SOURCE_FILE 2>&1`;

    if [ $? -eq 0 ]; then
        echo "ERROR: latc succeed!"
    else
        echo "OK"
        echo_verbose $VERBOSE "$LATC_OUTPUT"
    fi;
}

function do_tests {
    DIR=$1

    for file in $DIR/*.lat; do
        print_in_line $file "`perform_test $file false`"
    done
}

function main {
    if [ $# -ne 0 ]; then
        SOURCE_FILE=$1

        perform_test $SOURCE_FILE true
    else
        for item in ${ALL_TESTS_DIRS[*]}; do
            do_tests $item
        done
    fi
}

make latc_llvm_codegen && main $*
