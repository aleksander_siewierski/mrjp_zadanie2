    .globl f3
f3:
.LFB2:
    push   %rbp
    mov    %rsp, %rbp
    sub    $32, %rsp
    # movl   12(%ebp), %eax
    movl   %edi, -8(%rbp)
    movl   $0, -16(%rbp)
    jmp .L13

.L20:
    movl   -16(%rbp), %eax
    lea    0(, %eax, 8), %rdx
    mov    %rsi, %rax
    add    %rdx, %rax
    movl   (%rax), %eax
    testl  %eax, %eax
    jne .L14
    movl   $0, -24(%rbp)
    jmp .L15

.L17:
    movl   -8(%rbp), %eax
    lea    0(, %eax, 8), %rdx
    mov    %rsi, %rax
    add    %rdx, %rax
    movl   (%rax), %eax
    movl   %eax, -24(%rbp)
    cmpl   $0, -24(%rbp)
    je .L15
    jmp .L16

.L15:
    subl   $1, -8(%rbp)
    movl   -8(%rbp), %eax
    cmpl   -16(%rbp), %eax
    jg .L17

.L16:
    cmpl   $0, -24(%rbp)
    jne .L18
    jmp .L12

.L18:
    movl   -16(%ebp), %eax
    lea    0(, %eax, 8), %rdx
    mov    %rsi, %rax
    add    %rax, %rdx
    movl   -24(%rbp), %eax
    movl   %eax, (%rdx)
    movl   -8(%rbp), %eax
    lea    0(, %eax, 8), %rdx
    mov    %rsi, %rax
    add    %rdx, %rax
    movl   $0, (%rax)

.L14:
    addl   $1, -16(%rbp)

.L13:
    movl   -16(%rbp), %eax
    cmpl   %edi, %eax
    jl .L20

.L12:
    leave
    ret
