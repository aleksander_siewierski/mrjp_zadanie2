    .globl f3
f3:
.LFB2:
    pushl  %ebp
    movl   %esp, %ebp
    subl   $16, %esp
    movl   12(%ebp), %eax
    movl   %eax, -4(%ebp)
    movl   $0, -8(%ebp)
    jmp .L13
.L20:
    movl   -8(%ebp), %eax
    leal   0(,%eax,4), %edx
    movl   8(%ebp), %eax
    addl   %edx, %eax
    movl   (%eax), %eax
    testl  %eax, %eax
    jne .L14
    movl   $0, -12(%ebp)
    jmp .L15
.L17:
    movl   -4(%ebp), %eax
    leal   0(,%eax,4), %edx
    movl   8(%ebp), %eax
    addl   %edx, %eax
    movl   (%eax), %eax
    movl   %eax, -12(%ebp)
    cmpl   $0, -12(%ebp)
    je .L15
    jmp .L16
.L15:
    subl   $1, -4(%ebp)
    movl   -4(%ebp), %eax
    cmpl   -8(%ebp), %eax
    jg .L17
.L16:
    cmpl   $0, -12(%ebp)
    jne .L18
    jmp .L12
.L18:
    movl   -8(%ebp), %eax
    leal   0(,%eax,4), %edx
    movl   8(%ebp), %eax
    addl   %eax, %edx
    movl   -12(%ebp), %eax
    movl   %eax, (%edx)
    movl   -4(%ebp), %eax
    leal   0(,%eax,4), %edx
    movl   8(%ebp), %eax
    addl   %edx, %eax
    movl   $0, (%eax)
.L14:
    addl   $1, -8(%ebp)
.L13:
    movl   -8(%ebp), %eax
    cmpl   12(%ebp), %eax
    jl .L20
.L12:
    leave
    ret
