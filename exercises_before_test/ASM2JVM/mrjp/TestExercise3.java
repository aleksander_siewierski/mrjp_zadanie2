
package mrjp;

import mrjp.Exercise3;

public class TestExercise3 {

    public static void main(String[] args) {
        int[] iList = {42, 11, 74, 101};
        int b = iList.length;

        System.out.println("START");

        Exercise3.f3(b, iList);

        for (int i = 0; i < iList.length; i++) {
            System.out.println(i + ": " + iList[i]);
        }

        System.out.println("END");
    }
}