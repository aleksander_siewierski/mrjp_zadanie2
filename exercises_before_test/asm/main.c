
int addInt(int, int);

int findElem(int[], int);

int printf(char*,...);

int main() {

    int list[4] = {1, 2, 3, 4};

    printf("%d\n", findElem(list, 0));
    printf("%d\n", findElem(list, 3));
    printf("%d\n", addInt(5, 16));
}
