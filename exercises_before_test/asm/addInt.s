
.globl addInt
.globl findElem

addInt:
    push   %rbp
    mov    %rsp, %rbp
    mov    %edi, %eax
    add    %esi, %eax
    # mov    %rax, %rdi
    # call   printInt
    mov    %rbp, %rsp
    pop    %rbp
    # leave
    ret

findElem:
    push   %rbp
    mov    %rsp, %rbp
    movl   (%rdi, %rsi, 4), %eax # 4th element of array on RDI
    mov    %rbp, %rsp
    pop    %rbp
    ret


LCO: .string "%d\n"

printInt:
    push %rbp
    mov %rsp, %rbp
    mov %rdi, %rsi
    mov $LCO, %rdi
    call printf
    leave
    ret
