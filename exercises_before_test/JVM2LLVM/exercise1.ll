@.testArray = private constant [4 x i32] [ i32 42, i32 11, i32 74, i32 101 ]
@.s0 = private constant [6 x i8] c"v0 = \00"
@.s1 = private constant [6 x i8] c"v1 = \00"
@.s2 = private constant [6 x i8] c"v2 = \00"

declare void @printInt(i32)
declare void @printString(i8*)

define i32 @main() {
    %.testArrayPtr = getelementptr [4 x i32]* @.testArray, i32 0, i32 0
    %.a1 = call i32* @f1(i32* %.testArrayPtr, i32 4)

    %.a1dr = load i32* %.a1
    call void @printInt(i32 %.a1dr)
    %.a2 = getelementptr i32* %.a1, i32 1
    %.a2dr = load i32* %.a2
    call void @printInt(i32 %.a2dr)
    %.a3 = getelementptr i32* %.a1, i32 2
    %.a3dr = load i32* %.a3
    call void @printInt(i32 %.a3dr)
    %.a4 = getelementptr i32* %.a1, i32 3
    %.a4dr = load i32* %.a4
    call void @printInt(i32 %.a4dr)

    ret i32 0
}

define i32* @f1(i32* %list, i32 %i) {
entry:
    ; v0 = 0, v1 = 0
    br label %label4

label4:
    %v1 = phi i32 [0, %entry], [%v1.2, %label44]
    %v0 = phi i32 [0, %entry], [%v0.1, %label44]

    %.s0p = getelementptr [6 x i8]* @.s0, i32 0, i32 0
    call void @printString(i8* %.s0p)
    call void @printInt(i32 %v0)
    %.s1p = getelementptr [6 x i8]* @.s1, i32 0, i32 0
    call void @printString(i8* %.s1p)
    call void @printInt(i32 %v1)

    %.t2 = icmp sge i32 %v1, %i
    br i1 %.t2, label %label47, label %label4b

label4b:
    %.t3 = icmp sge i32 %v0, %i
    br i1 %.t3, label %label21, label %label4c

label4c:
    %.t4 = getelementptr i32* %list, i32 %v0
    %.t4b = load i32* %.t4
    br label %label21

label21:
    %v2 = phi i32 [0, %label4b], [%.t4b, %label4c]

    %.s2p = getelementptr [6 x i8]* @.s2, i32 0, i32 0
    call void @printString(i8* %.s2p)
    call void @printInt(i32 %v2)

    %.t5 = icmp sge i32 %v0, %i
    %v0.1 = add i32 %v0, 1
    br i1 %.t5, label %label36, label %label21b

label21b:
    %.t6 = icmp eq i32 %v2, 0
    br i1 %.t6, label %label44, label %label36

label36:
    %.t7 = getelementptr i32* %list, i32 %v1
    %v1.1 = add i32 %v1, 1
    store i32 %v2, i32* %.t7
    br label %label44

label44:
    %v1.2 = phi i32 [%v1, %label21b], [%v1.1, %label36]
    br label %label4

label47:
    %.t8 = getelementptr i32* %list, i32 0
    ret i32* %.t8
}
