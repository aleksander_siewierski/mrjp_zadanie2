.source Exercise1.j
.class  public mrjp/Exercise1
.super  java/lang/Object

; standard initializer
.method public <init>()V
    aload_0
    invokenonvirtual java/lang/Object/<init>()V
    return
.end method

.method public static main([Ljava/lang/String;)V
    .limit locals 1
    .limit stack 0
    return
.end method

.method public static f1([II)[I
    .limit stack 3
    .limit locals 5
    0: iconst_0
    1: istore_2
    2: iload_2
    3: istore_3
    iconst_0
    istore 4
Label4:
    getstatic java/lang/System/out Ljava/io/PrintStream;
    ldc "i = "
    invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
    getstatic java/lang/System/out Ljava/io/PrintStream;
    iload 2
    invokevirtual java/io/PrintStream/print(I)V
    getstatic java/lang/System/out Ljava/io/PrintStream;
    ldc ", j = "
    invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
    getstatic java/lang/System/out Ljava/io/PrintStream;
    iload 3
    invokevirtual java/io/PrintStream/print(I)V
    getstatic java/lang/System/out Ljava/io/PrintStream;
    ldc ", k = "
    invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
    getstatic java/lang/System/out Ljava/io/PrintStream;
    iload 4
    invokevirtual java/io/PrintStream/println(I)V
    4: iload_3
    5: iload_1
    6: if_icmpge Label47
    9: iload_2
    10: iload_1
    11: if_icmpge Label20
    14: aload_0
    15: iload_2
    16: iaload
    17: goto Label21
Label20:
    20: iconst_0
Label21:
    21: istore 4
    23: iload_2
    24: iinc 2 1
    27: iload_1
    28: if_icmpge Label36
    31: iload 4
    33: ifeq Label44
Label36:
    36: aload_0
    37: iload_3
    38: iinc 3 1
    41: iload 4
    43: iastore
Label44:
    44: goto Label4
Label47:
    45: aload 0
    47: areturn
.end method
