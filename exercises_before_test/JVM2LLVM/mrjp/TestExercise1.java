
package mrjp;

import mrjp.Exercise1;

public class TestExercise1 {

    public static void main(String[] args) {
        int[] iList = {10, 8, -6, 22};
        int b = iList.length;

        System.out.println("START");

        int[] iL2 = Exercise1.f1(iList, b);

        for (int i = 0; i < iL2.length; i++) {
            System.out.println(i + ": " + iL2[i]);
        }

        System.out.println("END");
    }
}