    .globl f2
f2:
.ENTRY:
    pushl  %ebp
    movl   %esp, %ebp
    subl   $20, %esp # %1 - -4(%ebp), %i - -8(%ebp), %j - -12(%ebp), %x - -16(%ebp)
    movl   12(%ebp), %eax
    movl   %eax, -4(%ebp)
    movl   8(%ebp), %eax
    movl   %eax, -12(%ebp)
    movl   %eax, -8(%ebp)
    jmp    .label4

.label4:
   # movl   -8(%ebp), %eax
    cmpl   -8(%ebp), $0
    jg .label7
    jmp .label20

.label7:
    movl   -8(%ebp), %eax
    subl   $1, %eax
    movl   %eax, -8(%ebp)
    movl   -4(%ebp), %edx
    leal   (%edx, %eax, 4), %edx
    movl   (%edx), %eax
    movl   %eax, -16(%ebp)
    cmpl   %eax, $0
    jne .label14
    jmp .label4

.label14:
    movl   -12(%ebp), %edx
    subl   $1, %edx
    movl   %edx, -12(%ebp)

    movl   -4(%ebp), %eax
    leal   (%eax, %edx, 4), %edx
    movl   -16(%ebp), %eax
    movl   %eax, (%edx)
    jmp .label4

.label20:
    movl   %ebp, %esp
    popl   %ebp
    ret
