@.testArray = private constant [4 x i32] [ i32 42, i32 11, i32 74, i32 101 ]

declare void @printInt(i32)
declare void @printString(i8*)

define i32 @main() {
    %.testArrayPtr = getelementptr [4 x i32]* @.testArray, i32 0, i32 0
    call void @f2(i32* %.testArrayPtr, i32 4)

    %.a1dr = load i32* %.testArrayPtr
    call void @printInt(i32 %.a1dr)
    %.a2 = getelementptr i32* %.testArrayPtr, i32 1
    %.a2dr = load i32* %.a2
    call void @printInt(i32 %.a2dr)
    %.a3 = getelementptr i32* %.testArrayPtr, i32 2
    %.a3dr = load i32* %.a3
    call void @printInt(i32 %.a3dr)
    %.a4 = getelementptr i32* %.testArrayPtr, i32 3
    %.a4dr = load i32* %.a4
    call void @printInt(i32 %.a4dr)

    ret i32 0
}

define void @f2(i32* %t, i32 %n) {
    %1 = alloca i32*
    %2 = alloca i32
    %i = alloca i32
    %j = alloca i32
    %x = alloca i32
    store i32* %t, i32** %1
    store i32 %n, i32* %2
    %3 = load i32* %2
    store i32 %3, i32* %j
    store i32 %3, i32* %i
    br label %4

; <label>:4
    %5 = load i32* %i
    %6 = icmp sgt i32 %5, 0
    br i1 %6, label %7, label %20

; <label>:7
    %8 = load i32* %i
    %9 = add i32 %8, -1
    store i32 %9, i32* %i
    %10 = load i32** %1
    %11 = getelementptr i32* %10, i32 %9
    %12 = load i32* %11
    store i32 %12, i32* %x
    %13 = icmp ne i32 %12, 0
    br i1 %13, label %14, label %4

; <label>:14
    %15 = load i32* %x
    %16 = load i32* %j
    %17 = add i32 %16, -1
    store i32 %17, i32* %j
    %18 = load i32** %1
    %19 = getelementptr i32* %18, i32 %17
    store i32 %15, i32* %19
    br label %4

; <label>:20
    ret void
}
