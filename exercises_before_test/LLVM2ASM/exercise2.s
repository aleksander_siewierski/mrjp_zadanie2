    .globl f2

f2:
.ENTRY:
    push   %rbp
    mov    %rsp, %rbp  # %1 - -4(%ebp), %i - -8(%ebp), %j - -12(%ebp), %x - -16(%ebp)
    sub    $40, %rsp # %1 - -8(%rbp), %i - -16(%rbp), %j - -24(%rbp), %x - -32(%rbp)
    mov    %rdi, %rax
    mov    %rax, -8(%rbp)
    movl   %esi, %eax
    movl   %eax, -24(%rbp)
    movl   %eax, -16(%rbp)
    jmp    .label4

.label4:
    # movl   -16(%rbp), %eax
    cmpl   $0, -16(%rbp)
    jl .label7
    jmp .label20

.label7:
    movl   -16(%rbp), %eax
    subl   $1, %eax
    movl   %eax, -16(%rbp)
    mov    -8(%rbp), %rdx
    lea    (%rdx, %rax, 8), %rdx
    movl   (%rdx), %eax
    movl   %eax, -32(%ebp)
    cmpl   $0, %eax
    jne .label14
    jmp .label4

.label14:
    movl   -24(%rbp), %edx
    subl   $1, %edx
    movl   %edx, -24(%rbp)

    mov    -8(%rbp), %rax
    # mov    -24(%rbp), %rdx # needed?
    lea    (%rax, %rdx, 8), %rdx
    movl   -32(%rbp), %eax
    movl   %eax, (%rdx)
    jmp .label4

.label20:
    mov   %rbp, %rsp
    pop   %rbp
    ret
